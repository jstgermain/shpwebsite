<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes = array(
        'homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\HomeController::homeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'about' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\AboutController::aboutAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/about',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'what_we_do' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\AboutController::whatWeDoAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/what-we-do',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mission_vision_values' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\AboutController::missionVisionValuesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/mission-vision-values',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'executive_team' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\AboutController::executiveTeamAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/executive-team',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'board_of_managers' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\AboutController::boardOfDirectorsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/board-of-managers',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        '_a_c_o' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ACOController::ACOAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/ACO',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        '_a_c_o_reporting' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ACOController::ACOReportingAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/ACO/PublicReporting',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'contact' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ContactController::contactAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/contact',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'news' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\NewsController::newsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'technology' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\TechnologyController::technologyAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/technology',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'contact_process' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\FormProcessController::contactProcessAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/process/contact',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'for_patients' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/for-patients',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'our_philosophy' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/our-philosophy',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'our_physicians' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhysiciansAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/our-physicians',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'physician_directory' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physicianDirectoryAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/physician-directory',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'physicians_in_practice' => array (  0 =>   array (    0 => 'practiceId',    1 => 'practiceLocationId',  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physiciansInPracticeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'practiceLocationId',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'practiceId',    ),    2 =>     array (      0 => 'text',      1 => '/physician-directory/practice',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'insurance_plans' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::insurancePlansAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/insurance-plans',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'care_management' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::careManagementAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/care-management',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'for_physicians' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPhysiciansController::forPhysiciansAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/for-physicians',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'hospitals' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\ForPatientsController::hospitalsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/hospitals',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api_all_practices' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\PhysicianAPIController::apiAllPracticesAction',    '_format' => NULL,  ),  2 =>   array (    '_method' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/api/physician-directory',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api_all_specialties' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\SpecialtiesAPIController::apiAllSpecialtiesAction',    '_format' => NULL,  ),  2 =>   array (    '_method' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/api/specialties',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        '_p_d_f_view' => array (  0 =>   array (    0 => 'file',  ),  1 =>   array (    '_controller' => 'SiteBundle\\Controller\\PDFViewerController::PDFViewAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'file',    ),    1 =>     array (      0 => 'text',      1 => '/pdf-viewer',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
