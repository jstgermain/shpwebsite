<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'SiteBundle\\Controller\\HomeController::homeAction',  '_route' => 'homepage',);
        }

        // about
        if ($pathinfo === '/about') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::aboutAction',  '_route' => 'about',);
        }

        // what_we_do
        if ($pathinfo === '/what-we-do') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::whatWeDoAction',  '_route' => 'what_we_do',);
        }

        // mission_vision_values
        if ($pathinfo === '/mission-vision-values') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::missionVisionValuesAction',  '_route' => 'mission_vision_values',);
        }

        // executive_team
        if ($pathinfo === '/executive-team') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::executiveTeamAction',  '_route' => 'executive_team',);
        }

        // board_of_managers
        if ($pathinfo === '/board-of-managers') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::boardOfDirectorsAction',  '_route' => 'board_of_managers',);
        }

        if (0 === strpos($pathinfo, '/ACO')) {
            // _a_c_o
            if ($pathinfo === '/ACO') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ACOController::ACOAction',  '_route' => '_a_c_o',);
            }

            // _a_c_o_reporting
            if ($pathinfo === '/ACO/PublicReporting') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ACOController::ACOReportingAction',  '_route' => '_a_c_o_reporting',);
            }

        }

        // contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ContactController::contactAction',  '_route' => 'contact',);
        }

        // news
        if ($pathinfo === '/news') {
            return array (  '_controller' => 'SiteBundle\\Controller\\NewsController::newsAction',  '_route' => 'news',);
        }

        // technology
        if ($pathinfo === '/technology') {
            return array (  '_controller' => 'SiteBundle\\Controller\\TechnologyController::technologyAction',  '_route' => 'technology',);
        }

        // contact_process
        if ($pathinfo === '/process/contact') {
            return array (  '_controller' => 'SiteBundle\\Controller\\FormProcessController::contactProcessAction',  '_route' => 'contact_process',);
        }

        // for_patients
        if ($pathinfo === '/for-patients') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  '_route' => 'for_patients',);
        }

        if (0 === strpos($pathinfo, '/our-ph')) {
            // our_philosophy
            if ($pathinfo === '/our-philosophy') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  '_route' => 'our_philosophy',);
            }

            // our_physicians
            if ($pathinfo === '/our-physicians') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhysiciansAction',  '_route' => 'our_physicians',);
            }

        }

        if (0 === strpos($pathinfo, '/physician-directory')) {
            // physician_directory
            if ($pathinfo === '/physician-directory') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physicianDirectoryAction',  '_route' => 'physician_directory',);
            }

            // physicians_in_practice
            if (0 === strpos($pathinfo, '/physician-directory/practice') && preg_match('#^/physician\\-directory/practice/(?P<practiceId>[^/]++)/(?P<practiceLocationId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'physicians_in_practice')), array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physiciansInPracticeAction',));
            }

        }

        // insurance_plans
        if ($pathinfo === '/insurance-plans') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::insurancePlansAction',  '_route' => 'insurance_plans',);
        }

        // care_management
        if ($pathinfo === '/care-management') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::careManagementAction',  '_route' => 'care_management',);
        }

        // for_physicians
        if ($pathinfo === '/for-physicians') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPhysiciansController::forPhysiciansAction',  '_route' => 'for_physicians',);
        }

        // hospitals
        if ($pathinfo === '/hospitals') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::hospitalsAction',  '_route' => 'hospitals',);
        }

        if (0 === strpos($pathinfo, '/api')) {
            // api_all_practices
            if ($pathinfo === '/api/physician-directory') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_all_practices;
                }

                return array (  '_controller' => 'SiteBundle\\Controller\\PhysicianAPIController::apiAllPracticesAction',  '_format' => NULL,  '_route' => 'api_all_practices',);
            }
            not_api_all_practices:

            // api_all_specialties
            if ($pathinfo === '/api/specialties') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_all_specialties;
                }

                return array (  '_controller' => 'SiteBundle\\Controller\\SpecialtiesAPIController::apiAllSpecialtiesAction',  '_format' => NULL,  '_route' => 'api_all_specialties',);
            }
            not_api_all_specialties:

        }

        // _p_d_f_view
        if (0 === strpos($pathinfo, '/pdf-viewer') && preg_match('#^/pdf\\-viewer/(?P<file>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_p_d_f_view')), array (  '_controller' => 'SiteBundle\\Controller\\PDFViewerController::PDFViewAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
