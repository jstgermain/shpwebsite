<?php

// ::js_footer_includes.html.twig
return array (
  '2a77e6d' => 
  array (
    0 => 
    array (
      0 => 'bundles/site/css/font-awesome.min.css',
      1 => 'bundles/site/css/bootstrap.min.css',
      2 => 'bundles/site/css/hover-dropdown-menu.css',
      3 => 'bundles/site/css/icons.css',
      4 => 'bundles/site/css/revolution-slider.css',
      5 => 'bundles/site/rs-plugin/css/settings.css',
      6 => 'bundles/site/css/animate.min.css',
      7 => 'bundles/site/css/owl/owl.carousel.css',
      8 => 'bundles/site/css/owl/owl.theme.css',
      9 => 'bundles/site/css/owl/owl.transitions.css',
      10 => 'bundles/site/css/prettyPhoto.css',
      11 => 'bundles/site/css/style.css',
      12 => 'bundles/site/css/responsive.css',
      13 => 'bundles/site/css/color.css',
      14 => 'bundles/site/css/bootstrap-helper.css',
    ),
    1 => 
    array (
      0 => 'uglifycss',
      1 => 'cssrewrite',
    ),
    2 => 
    array (
      'output' => 'css/2a77e6d.css',
      'name' => '2a77e6d',
      'debug' => NULL,
      'combine' => NULL,
      'vars' => 
      array (
      ),
    ),
  ),
  '0d8c8b0' => 
  array (
    0 => 
    array (
      0 => 'bundles/site/js/bootstrap.min.js',
      1 => 'bundles/site/js/hover-dropdown-menu.js',
      2 => 'bundles/site/js/jquery.hover-dropdown-menu-addon.js',
      3 => 'bundles/site/js/jquery.easing.1.3.js',
      4 => 'bundles/site/js/jquery.sticky.js',
      5 => 'bundles/site/js/bootstrapValidator.min.js',
      6 => 'bundles/site/rs-plugin/js/jquery.themepunch.tools.min.js',
      7 => 'bundles/site/rs-plugin/js/jquery.themepunch.revolution.min.js',
      8 => 'bundles/site/js/revolution-custom.js',
      9 => 'bundles/site/js/jquery.mixitup.min.js',
      10 => 'bundles/site/js/jquery.appear.js',
      11 => 'bundles/site/js/effect.js',
      12 => 'bundles/site/js/owl.carousel.min.js',
      13 => 'bundles/site/js/jquery.prettyPhoto.js',
      14 => 'bundles/site/js/jquery.parallax-1.1.3.js',
      15 => 'bundles/site/js/jquery.countTo.js',
      16 => 'bundles/site/js/jquery.mb.YTPlayer.js',
      17 => 'bundles/site/js/custom.js',
    ),
    1 => 
    array (
      0 => 'uglifyjs2',
    ),
    2 => 
    array (
      'output' => 'js/0d8c8b0.js',
      'name' => '0d8c8b0',
      'debug' => NULL,
      'combine' => NULL,
      'vars' => 
      array (
      ),
    ),
  ),
);
