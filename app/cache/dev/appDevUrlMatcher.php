<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/2a77e6d')) {
            // _assetic_2a77e6d
            if ($pathinfo === '/css/2a77e6d.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_2a77e6d',);
            }

            if (0 === strpos($pathinfo, '/css/2a77e6d_')) {
                // _assetic_2a77e6d_0
                if ($pathinfo === '/css/2a77e6d_font-awesome.min_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_0',);
                }

                // _assetic_2a77e6d_1
                if ($pathinfo === '/css/2a77e6d_bootstrap.min_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_1',);
                }

                // _assetic_2a77e6d_2
                if ($pathinfo === '/css/2a77e6d_hover-dropdown-menu_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_2',);
                }

                // _assetic_2a77e6d_3
                if ($pathinfo === '/css/2a77e6d_icons_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_3',);
                }

                // _assetic_2a77e6d_4
                if ($pathinfo === '/css/2a77e6d_revolution-slider_5.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_4',);
                }

                // _assetic_2a77e6d_5
                if ($pathinfo === '/css/2a77e6d_settings_6.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_5',);
                }

                // _assetic_2a77e6d_6
                if ($pathinfo === '/css/2a77e6d_animate.min_7.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 6,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_6',);
                }

                if (0 === strpos($pathinfo, '/css/2a77e6d_owl.')) {
                    // _assetic_2a77e6d_7
                    if ($pathinfo === '/css/2a77e6d_owl.carousel_8.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 7,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_7',);
                    }

                    if (0 === strpos($pathinfo, '/css/2a77e6d_owl.t')) {
                        // _assetic_2a77e6d_8
                        if ($pathinfo === '/css/2a77e6d_owl.theme_9.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 8,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_8',);
                        }

                        // _assetic_2a77e6d_9
                        if ($pathinfo === '/css/2a77e6d_owl.transitions_10.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 9,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_9',);
                        }

                    }

                }

                // _assetic_2a77e6d_10
                if ($pathinfo === '/css/2a77e6d_prettyPhoto_11.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 10,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_10',);
                }

                // _assetic_2a77e6d_11
                if ($pathinfo === '/css/2a77e6d_style_12.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 11,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_11',);
                }

                // _assetic_2a77e6d_12
                if ($pathinfo === '/css/2a77e6d_responsive_13.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 12,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_12',);
                }

                // _assetic_2a77e6d_13
                if ($pathinfo === '/css/2a77e6d_color_14.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 13,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_13',);
                }

                // _assetic_2a77e6d_14
                if ($pathinfo === '/css/2a77e6d_bootstrap-helper_15.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2a77e6d',  'pos' => 14,  '_format' => 'css',  '_route' => '_assetic_2a77e6d_14',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/0d8c8b0')) {
            // _assetic_0d8c8b0
            if ($pathinfo === '/js/0d8c8b0.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0',);
            }

            if (0 === strpos($pathinfo, '/js/0d8c8b0_')) {
                // _assetic_0d8c8b0_0
                if ($pathinfo === '/js/0d8c8b0_bootstrap.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_0',);
                }

                // _assetic_0d8c8b0_1
                if ($pathinfo === '/js/0d8c8b0_hover-dropdown-menu_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_1',);
                }

                if (0 === strpos($pathinfo, '/js/0d8c8b0_jquery.')) {
                    // _assetic_0d8c8b0_2
                    if ($pathinfo === '/js/0d8c8b0_jquery.hover-dropdown-menu-addon_3.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_2',);
                    }

                    // _assetic_0d8c8b0_3
                    if ($pathinfo === '/js/0d8c8b0_jquery.easing.1.3_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_3',);
                    }

                    // _assetic_0d8c8b0_4
                    if ($pathinfo === '/js/0d8c8b0_jquery.sticky_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_4',);
                    }

                }

                // _assetic_0d8c8b0_5
                if ($pathinfo === '/js/0d8c8b0_bootstrapValidator.min_6.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_5',);
                }

                if (0 === strpos($pathinfo, '/js/0d8c8b0_jquery.themepunch.')) {
                    // _assetic_0d8c8b0_6
                    if ($pathinfo === '/js/0d8c8b0_jquery.themepunch.tools.min_7.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_6',);
                    }

                    // _assetic_0d8c8b0_7
                    if ($pathinfo === '/js/0d8c8b0_jquery.themepunch.revolution.min_8.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_7',);
                    }

                }

                // _assetic_0d8c8b0_8
                if ($pathinfo === '/js/0d8c8b0_revolution-custom_9.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_8',);
                }

                if (0 === strpos($pathinfo, '/js/0d8c8b0_jquery.')) {
                    // _assetic_0d8c8b0_9
                    if ($pathinfo === '/js/0d8c8b0_jquery.mixitup.min_10.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_9',);
                    }

                    // _assetic_0d8c8b0_10
                    if ($pathinfo === '/js/0d8c8b0_jquery.appear_11.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_10',);
                    }

                }

                // _assetic_0d8c8b0_11
                if ($pathinfo === '/js/0d8c8b0_effect_12.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_11',);
                }

                // _assetic_0d8c8b0_12
                if ($pathinfo === '/js/0d8c8b0_owl.carousel.min_13.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_12',);
                }

                if (0 === strpos($pathinfo, '/js/0d8c8b0_jquery.')) {
                    if (0 === strpos($pathinfo, '/js/0d8c8b0_jquery.p')) {
                        // _assetic_0d8c8b0_13
                        if ($pathinfo === '/js/0d8c8b0_jquery.prettyPhoto_14.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_13',);
                        }

                        // _assetic_0d8c8b0_14
                        if ($pathinfo === '/js/0d8c8b0_jquery.parallax-1.1.3_15.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 14,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_14',);
                        }

                    }

                    // _assetic_0d8c8b0_15
                    if ($pathinfo === '/js/0d8c8b0_jquery.countTo_16.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 15,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_15',);
                    }

                    // _assetic_0d8c8b0_16
                    if ($pathinfo === '/js/0d8c8b0_jquery.mb.YTPlayer_17.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 16,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_16',);
                    }

                }

                // _assetic_0d8c8b0_17
                if ($pathinfo === '/js/0d8c8b0_custom_18.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '0d8c8b0',  'pos' => 17,  '_format' => 'js',  '_route' => '_assetic_0d8c8b0_17',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'SiteBundle\\Controller\\HomeController::homeAction',  '_route' => 'homepage',);
        }

        // about
        if ($pathinfo === '/about') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::aboutAction',  '_route' => 'about',);
        }

        // what_we_do
        if ($pathinfo === '/what-we-do') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::whatWeDoAction',  '_route' => 'what_we_do',);
        }

        // mission_vision_values
        if ($pathinfo === '/mission-vision-values') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::missionVisionValuesAction',  '_route' => 'mission_vision_values',);
        }

        // executive_team
        if ($pathinfo === '/executive-team') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::executiveTeamAction',  '_route' => 'executive_team',);
        }

        // board_of_managers
        if ($pathinfo === '/board-of-managers') {
            return array (  '_controller' => 'SiteBundle\\Controller\\AboutController::boardOfDirectorsAction',  '_route' => 'board_of_managers',);
        }

        if (0 === strpos($pathinfo, '/ACO')) {
            // _a_c_o
            if ($pathinfo === '/ACO') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ACOController::ACOAction',  '_route' => '_a_c_o',);
            }

            // _a_c_o_reporting
            if ($pathinfo === '/ACO/PublicReporting') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ACOController::ACOReportingAction',  '_route' => '_a_c_o_reporting',);
            }

        }

        // contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ContactController::contactAction',  '_route' => 'contact',);
        }

        // news
        if ($pathinfo === '/news') {
            return array (  '_controller' => 'SiteBundle\\Controller\\NewsController::newsAction',  '_route' => 'news',);
        }

        // technology
        if ($pathinfo === '/technology') {
            return array (  '_controller' => 'SiteBundle\\Controller\\TechnologyController::technologyAction',  '_route' => 'technology',);
        }

        // contact_process
        if ($pathinfo === '/process/contact') {
            return array (  '_controller' => 'SiteBundle\\Controller\\FormProcessController::contactProcessAction',  '_route' => 'contact_process',);
        }

        // for_patients
        if ($pathinfo === '/for-patients') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  '_route' => 'for_patients',);
        }

        if (0 === strpos($pathinfo, '/our-ph')) {
            // our_philosophy
            if ($pathinfo === '/our-philosophy') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhilosophyAction',  '_route' => 'our_philosophy',);
            }

            // our_physicians
            if ($pathinfo === '/our-physicians') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::ourPhysiciansAction',  '_route' => 'our_physicians',);
            }

        }

        if (0 === strpos($pathinfo, '/physician-directory')) {
            // physician_directory
            if ($pathinfo === '/physician-directory') {
                return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physicianDirectoryAction',  '_route' => 'physician_directory',);
            }

            // physicians_in_practice
            if (0 === strpos($pathinfo, '/physician-directory/practice') && preg_match('#^/physician\\-directory/practice/(?P<practiceId>[^/]++)/(?P<practiceLocationId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'physicians_in_practice')), array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::physiciansInPracticeAction',));
            }

        }

        // insurance_plans
        if ($pathinfo === '/insurance-plans') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::insurancePlansAction',  '_route' => 'insurance_plans',);
        }

        // care_management
        if ($pathinfo === '/care-management') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::careManagementAction',  '_route' => 'care_management',);
        }

        // for_physicians
        if ($pathinfo === '/for-physicians') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPhysiciansController::forPhysiciansAction',  '_route' => 'for_physicians',);
        }

        // hospitals
        if ($pathinfo === '/hospitals') {
            return array (  '_controller' => 'SiteBundle\\Controller\\ForPatientsController::hospitalsAction',  '_route' => 'hospitals',);
        }

        if (0 === strpos($pathinfo, '/api')) {
            // api_all_practices
            if ($pathinfo === '/api/physician-directory') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_all_practices;
                }

                return array (  '_controller' => 'SiteBundle\\Controller\\PhysicianAPIController::apiAllPracticesAction',  '_format' => NULL,  '_route' => 'api_all_practices',);
            }
            not_api_all_practices:

            // api_all_specialties
            if ($pathinfo === '/api/specialties') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_all_specialties;
                }

                return array (  '_controller' => 'SiteBundle\\Controller\\SpecialtiesAPIController::apiAllSpecialtiesAction',  '_format' => NULL,  '_route' => 'api_all_specialties',);
            }
            not_api_all_specialties:

        }

        // _p_d_f_view
        if (0 === strpos($pathinfo, '/pdf-viewer') && preg_match('#^/pdf\\-viewer/(?P<file>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_p_d_f_view')), array (  '_controller' => 'SiteBundle\\Controller\\PDFViewerController::PDFViewAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
