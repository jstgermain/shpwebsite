<?php

/* ::header.html.twig */
class __TwigTemplate_d33f5c0204a29c6ad6ad369ca53428c648ce7b1cab5f66e437483e24e4470373 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6a1fe70405daa7887a00fb9832e480781b40649a785e80433aad5563de16582 = $this->env->getExtension("native_profiler");
        $__internal_d6a1fe70405daa7887a00fb9832e480781b40649a785e80433aad5563de16582->enter($__internal_d6a1fe70405daa7887a00fb9832e480781b40649a785e80433aad5563de16582_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::header.html.twig"));

        // line 1
        $this->displayBlock('header', $context, $blocks);
        
        $__internal_d6a1fe70405daa7887a00fb9832e480781b40649a785e80433aad5563de16582->leave($__internal_d6a1fe70405daa7887a00fb9832e480781b40649a785e80433aad5563de16582_prof);

    }

    public function block_header($context, array $blocks = array())
    {
        $__internal_923527aa43a438182608a6894687da970b5ebbeb0b83b7a90924d38acaa545b7 = $this->env->getExtension("native_profiler");
        $__internal_923527aa43a438182608a6894687da970b5ebbeb0b83b7a90924d38acaa545b7->enter($__internal_923527aa43a438182608a6894687da970b5ebbeb0b83b7a90924d38acaa545b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 2
        echo "
<head>
\t<meta charset=\"UTF-8\">
\t<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
\t<meta name=\"keywords\" content=\"";
        // line 6
        if ((array_key_exists("meta_keywords", $context) && (twig_length_filter($this->env, (isset($context["meta_keywords"]) ? $context["meta_keywords"] : $this->getContext($context, "meta_keywords"))) > 0))) {
            echo twig_escape_filter($this->env, (isset($context["meta_keywords"]) ? $context["meta_keywords"] : $this->getContext($context, "meta_keywords")), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, (isset($context["meta_keywords_default"]) ? $context["meta_keywords_default"] : $this->getContext($context, "meta_keywords_default")), "html", null, true);
        }
        echo "\" />
\t<meta name=\"description\" content=\"";
        // line 7
        if ((array_key_exists("meta_description", $context) && (twig_length_filter($this->env, (isset($context["meta_description"]) ? $context["meta_description"] : $this->getContext($context, "meta_description"))) > 0))) {
            echo twig_escape_filter($this->env, (isset($context["meta_description"]) ? $context["meta_description"] : $this->getContext($context, "meta_description")), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, (isset($context["meta_description_default"]) ? $context["meta_description_default"] : $this->getContext($context, "meta_description_default")), "html", null, true);
        }
        echo "\" />
\t<meta name=\"author\" content=\"";
        // line 8
        if ((array_key_exists("meta_author", $context) && (twig_length_filter($this->env, (isset($context["meta_author"]) ? $context["meta_author"] : $this->getContext($context, "meta_author"))) > 0))) {
            echo twig_escape_filter($this->env, (isset($context["meta_author"]) ? $context["meta_author"] : $this->getContext($context, "meta_author")), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, (isset($context["meta_author_default"]) ? $context["meta_author_default"] : $this->getContext($context, "meta_author_default")), "html", null, true);
        }
        echo "\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />

\t<title>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        if (array_key_exists("title", $context)) {
            echo " | ";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        }
        echo "</title>

\t<!-- Favicon -->
\t<link rel=\"shortcut icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-57x57.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-60x60.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-72x72.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-76x76.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-114x114.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-120x120.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-144x144.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-152x152.png"), "html", null, true);
        echo "\" />
\t<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("apple-touch-icon-180x180.png"), "html", null, true);
        echo "\" />
\t<link rel=\"icon\" type=\"image/png\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon-32x32.png"), "html", null, true);
        echo "\" sizes=\"32x32\" />
\t<link rel=\"icon\" type=\"image/png\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("android-chrome-192x192.png"), "html", null, true);
        echo "\" sizes=\"192x192\" />
\t<link rel=\"icon\" type=\"image/png\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon-96x96.png"), "html", null, true);
        echo "\" sizes=\"96x96\" />
\t<link rel=\"icon\" type=\"image/png\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon-16x16.png"), "html", null, true);
        echo "\" sizes=\"16x16\" />
\t<link rel=\"manifest\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("manifest.json"), "html", null, true);
        echo "\" />
\t<meta name=\"msapplication-TileColor\" content=\"#0c0562\" />
\t<meta name=\"msapplication-TileImage\" content=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("mstile-144x144.png"), "html", null, true);
        echo "\" />
\t<meta name=\"theme-color\" content=\"#ffffff\" />


\t<!-- Font -->
\t<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic' />
\t<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />

\t<!-- Core CSS Files -->

\t";
        // line 40
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "2a77e6d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_font-awesome.min_1.css");
            // line 57
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_hover-dropdown-menu_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_3") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_icons_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_4") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_revolution-slider_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_5") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_settings_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_6") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_animate.min_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_7") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_owl.carousel_8.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_8") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_owl.theme_9.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_9") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_owl.transitions_10.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_10"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_10") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_prettyPhoto_11.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_11"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_11") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_style_12.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_12"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_12") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_responsive_13.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_13"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_13") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_color_14.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "2a77e6d_14"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d_14") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d_bootstrap-helper_15.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "2a77e6d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2a77e6d") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/2a77e6d.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 59
        echo "
\t";
        // line 60
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 61
        echo "
</head>
";
        
        $__internal_923527aa43a438182608a6894687da970b5ebbeb0b83b7a90924d38acaa545b7->leave($__internal_923527aa43a438182608a6894687da970b5ebbeb0b83b7a90924d38acaa545b7_prof);

    }

    // line 60
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ddbd875baa0b5fb6d7af784428a74a76e183bf539800909167cdc5efaa0d3112 = $this->env->getExtension("native_profiler");
        $__internal_ddbd875baa0b5fb6d7af784428a74a76e183bf539800909167cdc5efaa0d3112->enter($__internal_ddbd875baa0b5fb6d7af784428a74a76e183bf539800909167cdc5efaa0d3112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ddbd875baa0b5fb6d7af784428a74a76e183bf539800909167cdc5efaa0d3112->leave($__internal_ddbd875baa0b5fb6d7af784428a74a76e183bf539800909167cdc5efaa0d3112_prof);

    }

    public function getTemplateName()
    {
        return "::header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  268 => 60,  259 => 61,  257 => 60,  254 => 59,  156 => 57,  152 => 40,  139 => 30,  134 => 28,  130 => 27,  126 => 26,  122 => 25,  118 => 24,  114 => 23,  110 => 22,  106 => 21,  102 => 20,  98 => 19,  94 => 18,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  68 => 11,  58 => 8,  50 => 7,  42 => 6,  36 => 2,  24 => 1,);
    }
}
/* {% block header %}*/
/* */
/* <head>*/
/* 	<meta charset="UTF-8">*/
/* 	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->*/
/* 	<meta name="keywords" content="{% if meta_keywords is defined and meta_keywords | length > 0 %}{{ meta_keywords }}{% else %}{{ meta_keywords_default }}{% endif %}" />*/
/* 	<meta name="description" content="{% if meta_description is defined and meta_description | length > 0 %}{{ meta_description }}{% else %}{{ meta_description_default }}{% endif %}" />*/
/* 	<meta name="author" content="{% if meta_author is defined and meta_author | length > 0 %}{{ meta_author }}{% else %}{{ meta_author_default }}{% endif %}" />*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />*/
/* */
/* 	<title>{{ company_name }}{% if title is defined %} | {{ title }}{% endif %}</title>*/
/* */
/* 	<!-- Favicon -->*/
/* 	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-touch-icon-57x57.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('apple-touch-icon-60x60.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-touch-icon-72x72.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-touch-icon-76x76.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon-114x114.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-touch-icon-120x120.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-touch-icon-144x144.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-touch-icon-152x152.png') }}" />*/
/* 	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon-180x180.png') }}" />*/
/* 	<link rel="icon" type="image/png" href="{{ asset('favicon-32x32.png') }}" sizes="32x32" />*/
/* 	<link rel="icon" type="image/png" href="{{ asset('android-chrome-192x192.png') }}" sizes="192x192" />*/
/* 	<link rel="icon" type="image/png" href="{{ asset('favicon-96x96.png') }}" sizes="96x96" />*/
/* 	<link rel="icon" type="image/png" href="{{ asset('favicon-16x16.png') }}" sizes="16x16" />*/
/* 	<link rel="manifest" href="{{ asset('manifest.json') }}" />*/
/* 	<meta name="msapplication-TileColor" content="#0c0562" />*/
/* 	<meta name="msapplication-TileImage" content="{{ asset('mstile-144x144.png') }}" />*/
/* 	<meta name="theme-color" content="#ffffff" />*/
/* */
/* */
/* 	<!-- Font -->*/
/* 	<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic' />*/
/* 	<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />*/
/* */
/* 	<!-- Core CSS Files -->*/
/* */
/* 	{% stylesheets*/
/* 	'bundles/site/css/font-awesome.min.css'*/
/* 	'bundles/site/css/bootstrap.min.css'*/
/* 	'bundles/site/css/hover-dropdown-menu.css'*/
/* 	'bundles/site/css/icons.css'*/
/* 	'bundles/site/css/revolution-slider.css'*/
/* 	'bundles/site/rs-plugin/css/settings.css'*/
/* 	'bundles/site/css/animate.min.css'*/
/* 	'bundles/site/css/owl/owl.carousel.css'*/
/* 	'bundles/site/css/owl/owl.theme.css'*/
/* 	'bundles/site/css/owl/owl.transitions.css'*/
/* 	'bundles/site/css/prettyPhoto.css'*/
/* 	'bundles/site/css/style.css'*/
/* 	'bundles/site/css/responsive.css'*/
/* 	'bundles/site/css/color.css'*/
/* 	'bundles/site/css/bootstrap-helper.css'*/
/* 	filter='uglifycss' filter='cssrewrite' %}*/
/* 	<link rel="stylesheet" href="{{ asset_url }}" />*/
/* 	{% endstylesheets %}*/
/* */
/* 	{% block stylesheets %}{% endblock %}*/
/* */
/* </head>*/
/* {% endblock %}*/
