<?php

/* knp_menu_base.html.twig */
class __TwigTemplate_146060ef92c723203fb0c91471249b31bec4f5f8c33db5e7ae99d36b9a2ef606 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bcf822def7a37d91c2dd5851fcfccad8c7f69865fc9b1c2a8a5bb87470a2da5 = $this->env->getExtension("native_profiler");
        $__internal_9bcf822def7a37d91c2dd5851fcfccad8c7f69865fc9b1c2a8a5bb87470a2da5->enter($__internal_9bcf822def7a37d91c2dd5851fcfccad8c7f69865fc9b1c2a8a5bb87470a2da5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "compressed", array())) {
            $this->displayBlock("compressed_root", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $__internal_9bcf822def7a37d91c2dd5851fcfccad8c7f69865fc9b1c2a8a5bb87470a2da5->leave($__internal_9bcf822def7a37d91c2dd5851fcfccad8c7f69865fc9b1c2a8a5bb87470a2da5_prof);

    }

    public function getTemplateName()
    {
        return "knp_menu_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% if options.compressed %}{{ block('compressed_root') }}{% else %}{{ block('root') }}{% endif %}*/
/* */
