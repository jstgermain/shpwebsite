<?php

/* ::js_footer_includes.html.twig */
class __TwigTemplate_172ef36dc667adfe9a8b5d38c4544b5c530f955e221ec5918d90df17819cff4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'js_footer_includes' => array($this, 'block_js_footer_includes'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_376fe872547832bd2dbaabf4e19208eacc922200fb2370d438e0039089e5dc0d = $this->env->getExtension("native_profiler");
        $__internal_376fe872547832bd2dbaabf4e19208eacc922200fb2370d438e0039089e5dc0d->enter($__internal_376fe872547832bd2dbaabf4e19208eacc922200fb2370d438e0039089e5dc0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::js_footer_includes.html.twig"));

        // line 1
        $this->displayBlock('js_footer_includes', $context, $blocks);
        
        $__internal_376fe872547832bd2dbaabf4e19208eacc922200fb2370d438e0039089e5dc0d->leave($__internal_376fe872547832bd2dbaabf4e19208eacc922200fb2370d438e0039089e5dc0d_prof);

    }

    public function block_js_footer_includes($context, array $blocks = array())
    {
        $__internal_e61f87a33c051ff0d5fd998795f08c625a0d692f44c8d6f930f02c8648a43c73 = $this->env->getExtension("native_profiler");
        $__internal_e61f87a33c051ff0d5fd998795f08c625a0d692f44c8d6f930f02c8648a43c73->enter($__internal_e61f87a33c051ff0d5fd998795f08c625a0d692f44c8d6f930f02c8648a43c73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js_footer_includes"));

        // line 2
        echo "
\t<!-- Scripts -->
\t<script src='//code.jquery.com/jquery-2.1.4.min.js'></script>

\t";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "0d8c8b0_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_bootstrap.min_1.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_hover-dropdown-menu_2.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.hover-dropdown-menu-addon_3.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_3") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.easing.1.3_4.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_4") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.sticky_5.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_5") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_bootstrapValidator.min_6.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_6") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.themepunch.tools.min_7.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_7") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.themepunch.revolution.min_8.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_8") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_revolution-custom_9.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_9") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.mixitup.min_10.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_10"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_10") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.appear_11.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_11"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_11") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_effect_12.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_12"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_12") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_owl.carousel.min_13.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_13"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_13") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.prettyPhoto_14.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_14"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_14") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.parallax-1.1.3_15.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_15"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_15") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.countTo_16.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_16"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_16") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_jquery.mb.YTPlayer_17.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
            // asset "0d8c8b0_17"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0_17") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0_custom_18.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        } else {
            // asset "0d8c8b0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_0d8c8b0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/0d8c8b0.js");
            // line 26
            echo "\t";
            // line 27
            echo "\t";
            // line 28
            echo "\t";
            // line 29
            echo "\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t";
        }
        unset($context["asset_url"]);
        // line 31
        echo "\t<!-- Scripts -->

";
        
        $__internal_e61f87a33c051ff0d5fd998795f08c625a0d692f44c8d6f930f02c8648a43c73->leave($__internal_e61f87a33c051ff0d5fd998795f08c625a0d692f44c8d6f930f02c8648a43c73_prof);

    }

    public function getTemplateName()
    {
        return "::js_footer_includes.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  293 => 31,  286 => 29,  284 => 28,  282 => 27,  280 => 26,  272 => 29,  270 => 28,  268 => 27,  266 => 26,  259 => 29,  257 => 28,  255 => 27,  253 => 26,  246 => 29,  244 => 28,  242 => 27,  240 => 26,  233 => 29,  231 => 28,  229 => 27,  227 => 26,  220 => 29,  218 => 28,  216 => 27,  214 => 26,  207 => 29,  205 => 28,  203 => 27,  201 => 26,  194 => 29,  192 => 28,  190 => 27,  188 => 26,  181 => 29,  179 => 28,  177 => 27,  175 => 26,  168 => 29,  166 => 28,  164 => 27,  162 => 26,  155 => 29,  153 => 28,  151 => 27,  149 => 26,  142 => 29,  140 => 28,  138 => 27,  136 => 26,  129 => 29,  127 => 28,  125 => 27,  123 => 26,  116 => 29,  114 => 28,  112 => 27,  110 => 26,  103 => 29,  101 => 28,  99 => 27,  97 => 26,  90 => 29,  88 => 28,  86 => 27,  84 => 26,  77 => 29,  75 => 28,  73 => 27,  71 => 26,  64 => 29,  62 => 28,  60 => 27,  58 => 26,  51 => 29,  49 => 28,  47 => 27,  45 => 26,  41 => 6,  35 => 2,  23 => 1,);
    }
}
/* {% block js_footer_includes %}*/
/* */
/* 	<!-- Scripts -->*/
/* 	<script src='//code.jquery.com/jquery-2.1.4.min.js'></script>*/
/* */
/* 	{% javascripts*/
/* 		'bundles/site/js/bootstrap.min.js'*/
/* 		'bundles/site/js/hover-dropdown-menu.js'*/
/* 		'bundles/site/js/jquery.hover-dropdown-menu-addon.js'*/
/* 		'bundles/site/js/jquery.easing.1.3.js'*/
/* 		'bundles/site/js/jquery.sticky.js'*/
/* 		'bundles/site/js/bootstrapValidator.min.js'*/
/* 		'bundles/site/rs-plugin/js/jquery.themepunch.tools.min.js'*/
/* 		'bundles/site/rs-plugin/js/jquery.themepunch.revolution.min.js'*/
/* 		'bundles/site/js/revolution-custom.js'*/
/* 		'bundles/site/js/jquery.mixitup.min.js'*/
/* 		'bundles/site/js/jquery.appear.js'*/
/* 		'bundles/site/js/effect.js'*/
/* 		'bundles/site/js/owl.carousel.min.js'*/
/* 		'bundles/site/js/jquery.prettyPhoto.js'*/
/* 		'bundles/site/js/jquery.parallax-1.1.3.js'*/
/* 		'bundles/site/js/jquery.countTo.js'*/
/* 		'bundles/site/js/jquery.mb.YTPlayer.js'*/
/* 		'bundles/site/js/custom.js'*/
/* 	filter='uglifyjs2' %}*/
/* 	{#'bundles/site/js/tweet/carousel.js'#}*/
/* 	{#'bundles/site/js/tweet/scripts.js'#}*/
/* 	{#'bundles/site/js/tweet/tweetie.min.js'#}*/
/* 	<script src="{{ asset_url }}"></script>*/
/* 	{% endjavascripts %}*/
/* 	<!-- Scripts -->*/
/* */
/* {% endblock %}*/
