<?php

/* ::base.html.twig */
class __TwigTemplate_187009bc03ad49c96c94333073141e6bdbf31e2724a1e9d30846b03ce8db556d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("::page_elements.html.twig", "::base.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."::page_elements.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $_trait_1 = $this->loadTemplate("::header.html.twig", "::base.html.twig", 2);
        // line 2
        if (!$_trait_1->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."::header.html.twig".'" cannot be used as a trait.');
        }
        $_trait_1_blocks = $_trait_1->getBlocks();

        $_trait_2 = $this->loadTemplate("::navigation.html.twig", "::base.html.twig", 3);
        // line 3
        if (!$_trait_2->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."::navigation.html.twig".'" cannot be used as a trait.');
        }
        $_trait_2_blocks = $_trait_2->getBlocks();

        $_trait_3 = $this->loadTemplate("::footer.html.twig", "::base.html.twig", 4);
        // line 4
        if (!$_trait_3->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."::footer.html.twig".'" cannot be used as a trait.');
        }
        $_trait_3_blocks = $_trait_3->getBlocks();

        $_trait_4 = $this->loadTemplate("::js_footer_includes.html.twig", "::base.html.twig", 5);
        // line 5
        if (!$_trait_4->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."::js_footer_includes.html.twig".'" cannot be used as a trait.');
        }
        $_trait_4_blocks = $_trait_4->getBlocks();

        $this->traits = array_merge(
            $_trait_0_blocks,
            $_trait_1_blocks,
            $_trait_2_blocks,
            $_trait_3_blocks,
            $_trait_4_blocks
        );

        $this->blocks = array_merge(
            $this->traits,
            array(
                'body' => array($this, 'block_body'),
                'javascripts' => array($this, 'block_javascripts'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4bb1146820b349321b9eb95c2cc9f2799b441353b830c4b7ab19ad17ab61f5c4 = $this->env->getExtension("native_profiler");
        $__internal_4bb1146820b349321b9eb95c2cc9f2799b441353b830c4b7ab19ad17ab61f5c4->enter($__internal_4bb1146820b349321b9eb95c2cc9f2799b441353b830c4b7ab19ad17ab61f5c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 6
        echo "
<!DOCTYPE html>
<html lang=\"en\">
";
        // line 9
        $this->displayBlock("header", $context, $blocks);
        echo "
<body>
<div id=\"page\">
\t<!-- Page Loader -->
\t<div id=\"pageloader\">
\t\t<div class=\"loader-item fa fa-spin text-color\"></div>
\t</div>

\t";
        // line 17
        $this->displayBlock("main_navigation", $context, $blocks);
        echo "

\t";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "
\t";
        // line 21
        $this->displayBlock("pe_our_locations_map", $context, $blocks);
        echo "
\t";
        // line 22
        $this->displayBlock("pe_contact_divider", $context, $blocks);
        echo "

\t";
        // line 24
        $this->displayBlock("footer", $context, $blocks);
        echo "

</div>
<!-- page -->

";
        // line 29
        $this->displayBlock("js_footer_includes", $context, $blocks);
        echo "

";
        // line 31
        $this->displayBlock('javascripts', $context, $blocks);
        // line 32
        echo "
";
        // line 33
        $this->displayBlock("pe_googe_tracking", $context, $blocks);
        echo "
";
        // line 34
        $this->displayBlock("pe_sypfu", $context, $blocks);
        echo "

</body>
</html>

";
        
        $__internal_4bb1146820b349321b9eb95c2cc9f2799b441353b830c4b7ab19ad17ab61f5c4->leave($__internal_4bb1146820b349321b9eb95c2cc9f2799b441353b830c4b7ab19ad17ab61f5c4_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_111596f71ddd64e798e914b0e7f50e119bec331c4dc4722a18576c144e038031 = $this->env->getExtension("native_profiler");
        $__internal_111596f71ddd64e798e914b0e7f50e119bec331c4dc4722a18576c144e038031->enter($__internal_111596f71ddd64e798e914b0e7f50e119bec331c4dc4722a18576c144e038031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_111596f71ddd64e798e914b0e7f50e119bec331c4dc4722a18576c144e038031->leave($__internal_111596f71ddd64e798e914b0e7f50e119bec331c4dc4722a18576c144e038031_prof);

    }

    // line 31
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a8b4c4d21f62d78f00e0f431538041d7513edfb67ff075cd7a3b07e61756f4fc = $this->env->getExtension("native_profiler");
        $__internal_a8b4c4d21f62d78f00e0f431538041d7513edfb67ff075cd7a3b07e61756f4fc->enter($__internal_a8b4c4d21f62d78f00e0f431538041d7513edfb67ff075cd7a3b07e61756f4fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_a8b4c4d21f62d78f00e0f431538041d7513edfb67ff075cd7a3b07e61756f4fc->leave($__internal_a8b4c4d21f62d78f00e0f431538041d7513edfb67ff075cd7a3b07e61756f4fc_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 31,  140 => 19,  127 => 34,  123 => 33,  120 => 32,  118 => 31,  113 => 29,  105 => 24,  100 => 22,  96 => 21,  93 => 20,  91 => 19,  86 => 17,  75 => 9,  70 => 6,  42 => 5,  35 => 4,  28 => 3,  21 => 2,  14 => 1,);
    }
}
/* {% use '::page_elements.html.twig' %}*/
/* {% use '::header.html.twig' %}*/
/* {% use '::navigation.html.twig' %}*/
/* {% use '::footer.html.twig' %}*/
/* {% use '::js_footer_includes.html.twig' %}*/
/* */
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* {{ block('header') }}*/
/* <body>*/
/* <div id="page">*/
/* 	<!-- Page Loader -->*/
/* 	<div id="pageloader">*/
/* 		<div class="loader-item fa fa-spin text-color"></div>*/
/* 	</div>*/
/* */
/* 	{{ block('main_navigation') }}*/
/* */
/* 	{% block body %}{% endblock %}*/
/* */
/* 	{{ block('pe_our_locations_map') }}*/
/* 	{{ block('pe_contact_divider') }}*/
/* */
/* 	{{ block('footer') }}*/
/* */
/* </div>*/
/* <!-- page -->*/
/* */
/* {{ block('js_footer_includes') }}*/
/* */
/* {% block javascripts %}{% endblock %}*/
/* */
/* {{ block('pe_googe_tracking') }}*/
/* {{ block('pe_sypfu') }}*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
