<?php

/* ::page_elements.html.twig */
class __TwigTemplate_6cba8f99d6474435b442ceb715d3c756b8446b60e5fc3f39d3878d8b096f5f0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'pe_home_slider' => array($this, 'block_pe_home_slider'),
            'pe_special_features' => array($this, 'block_pe_special_features'),
            'pe_who_we_are' => array($this, 'block_pe_who_we_are'),
            'pe_patient_testimonials' => array($this, 'block_pe_patient_testimonials'),
            'pe_physician_testimonials' => array($this, 'block_pe_physician_testimonials'),
            'pe_counters' => array($this, 'block_pe_counters'),
            'pe_google_map' => array($this, 'block_pe_google_map'),
            'pe_contact_divider' => array($this, 'block_pe_contact_divider'),
            'pe_our_locations_map' => array($this, 'block_pe_our_locations_map'),
            'pe_footer_news' => array($this, 'block_pe_footer_news'),
            'pe_googe_tracking' => array($this, 'block_pe_googe_tracking'),
            'pe_sypfu' => array($this, 'block_pe_sypfu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f407ae41cf0290c8206d9d3394b18d9eb3aedeb9b442b308b3167984faf6bc54 = $this->env->getExtension("native_profiler");
        $__internal_f407ae41cf0290c8206d9d3394b18d9eb3aedeb9b442b308b3167984faf6bc54->enter($__internal_f407ae41cf0290c8206d9d3394b18d9eb3aedeb9b442b308b3167984faf6bc54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::page_elements.html.twig"));

        // line 2
        $this->displayBlock('pe_home_slider', $context, $blocks);
        // line 122
        echo "

";
        // line 125
        $this->displayBlock('pe_special_features', $context, $blocks);
        // line 210
        echo "

";
        // line 213
        $this->displayBlock('pe_who_we_are', $context, $blocks);
        // line 252
        echo "

";
        // line 255
        $this->displayBlock('pe_patient_testimonials', $context, $blocks);
        // line 337
        echo "

";
        // line 340
        $this->displayBlock('pe_physician_testimonials', $context, $blocks);
        // line 422
        echo "

";
        // line 425
        $this->displayBlock('pe_counters', $context, $blocks);
        // line 459
        echo "

";
        // line 462
        $this->displayBlock('pe_google_map', $context, $blocks);
        // line 480
        echo "
";
        // line 481
        $this->displayBlock('pe_contact_divider', $context, $blocks);
        // line 492
        echo "
";
        // line 493
        $this->displayBlock('pe_our_locations_map', $context, $blocks);
        // line 510
        echo "
";
        // line 511
        $this->displayBlock('pe_footer_news', $context, $blocks);
        // line 527
        echo "
";
        // line 528
        $this->displayBlock('pe_googe_tracking', $context, $blocks);
        // line 540
        echo "
";
        // line 541
        $this->displayBlock('pe_sypfu', $context, $blocks);
        
        $__internal_f407ae41cf0290c8206d9d3394b18d9eb3aedeb9b442b308b3167984faf6bc54->leave($__internal_f407ae41cf0290c8206d9d3394b18d9eb3aedeb9b442b308b3167984faf6bc54_prof);

    }

    // line 2
    public function block_pe_home_slider($context, array $blocks = array())
    {
        $__internal_16fca72ae44b2e22d20b373f7623d462ba9694d8976a814eeb5446ad27228c43 = $this->env->getExtension("native_profiler");
        $__internal_16fca72ae44b2e22d20b373f7623d462ba9694d8976a814eeb5446ad27228c43->enter($__internal_16fca72ae44b2e22d20b373f7623d462ba9694d8976a814eeb5446ad27228c43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_home_slider"));

        // line 3
        echo "\t<section class=\"slider\">
\t\t<div class=\"tp-banner\">
\t\t\t<ul>
\t\t\t\t<!-- Slide -->
\t\t\t\t<li data-delay=\"7000\" class=\"bg-white\" data-transition=\"fade\" data-slotamount=\"7\" data-start-height=\"300\" data-masterspeed=\"2000\">
\t\t\t\t\t<div class=\"elements\">
\t\t\t\t\t\t<div class=\"tp-caption tp-resizeme lfb\"
\t\t\t\t\t\t\t data-x=\"450\"
\t\t\t\t\t\t\t data-y=\"95\"
\t\t\t\t\t\t\t data-speed=\"1000\"
\t\t\t\t\t\t\t data-start=\"2040\"
\t\t\t\t\t\t\t data-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\t data-endspeed=\"500\"
\t\t\t\t\t\t\t data-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\t style=\"z-index: 4; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/slider/slide-1.png"), "html", null, true);
        echo "\" alt=\"\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h2 class=\"tp-caption lft skewtotop title bold\"
\t\t\t\t\t\t\tdata-x=\"12\"
\t\t\t\t\t\t\tdata-y=\"231\"
\t\t\t\t\t\t\tdata-speed=\"1000\"
\t\t\t\t\t\t\tdata-start=\"1700\"
\t\t\t\t\t\t\tdata-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\tdata-endspeed=\"500\"
\t\t\t\t\t\t\tdata-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\tstyle=\"text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<strong><span class=\"text-color\">Find</span> a <br>Physician</strong>
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t<div class=\"tp-caption lfr skewtoright description text-left hidden-xs\"
\t\t\t\t\t\t\t data-x=\"12\"
\t\t\t\t\t\t\t data-y=\"400\"
\t\t\t\t\t\t\t data-speed=\"1000\"
\t\t\t\t\t\t\t data-start=\"1900\"
\t\t\t\t\t\t\t data-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\t data-endspeed=\"500\"
\t\t\t\t\t\t\t data-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\t style=\"max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<h4><a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("our_physicians");
        echo "\"><span style=\"background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;\">Learn More</span></a></h4>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/slider/slide-1-back.jpg"), "html", null, true);
        echo "\" alt=\"\" data-bgfit=\"cover\" data-bgposition=\"center top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t</li>
\t\t\t\t<!-- Slide Ends -->
\t\t\t\t<!-- Slide -->
\t\t\t\t<li data-delay=\"7000\" data-transition=\"fade\" data-slotamount=\"7\" data-start-height=\"300\" data-masterspeed=\"2000\">
\t\t\t\t\t<div class=\"elements\">
\t\t\t\t\t\t<h2 class=\"tp-caption lft skewtotop title bold\"
\t\t\t\t\t\t\tdata-x=\"12\"
\t\t\t\t\t\t\tdata-y=\"151\"
\t\t\t\t\t\t\tdata-speed=\"1000\"
\t\t\t\t\t\t\tdata-start=\"1700\"
\t\t\t\t\t\t\tdata-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\tdata-endspeed=\"500\"
\t\t\t\t\t\t\tdata-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\tstyle=\"text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<strong>Setting the standard <span class=\"text-color\">for excellence</span><br/>in patient-centered care</strong>
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t<div class=\"tp-caption lfr skewtoright description text-left hidden-xs\"
\t\t\t\t\t\t\t data-x=\"20\"
\t\t\t\t\t\t\t data-y=\"350\"
\t\t\t\t\t\t\t data-speed=\"1000\"
\t\t\t\t\t\t\t data-start=\"1900\"
\t\t\t\t\t\t\t data-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\t data-endspeed=\"500\"
\t\t\t\t\t\t\t data-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\t style=\"max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<h4>Our Care Coordinators and Transitional Care Managers are here to support you every step of the way<br/>
\t\t\t\t\t\t\t<a href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("care_management");
        echo "\"><span style=\"background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;\">Learn More</span></a></>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/slider/slide-2.jpg"), "html", null, true);
        echo "\" alt=\"\" data-bgfit=\"cover\" data-bgposition=\"center top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t</li>
\t\t\t\t<!-- Slide Ends -->
\t\t\t\t<!-- Slide -->
\t\t\t\t<li data-delay=\"7000\" class=\"bg-white\" data-transition=\"fade\" data-slotamount=\"7\" data-start-height=\"300\" data-masterspeed=\"2000\">
\t\t\t\t\t<div class=\"elements\">
\t\t\t\t\t\t<div class=\"tp-caption tp-resizeme lfb\"
\t\t\t\t\t\t\t data-x=\"12\"
\t\t\t\t\t\t\t data-y=\"85\"
\t\t\t\t\t\t\t data-speed=\"1000\"
\t\t\t\t\t\t\t data-start=\"2040\"
\t\t\t\t\t\t\t data-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\t data-endspeed=\"500\"
\t\t\t\t\t\t\t data-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\t style=\"z-index: 4; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<img src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/slider/slide-3.png"), "html", null, true);
        echo "\" width=\"500\" alt=\"\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<h2 class=\"tp-caption lft skewtotop title bold\"
\t\t\t\t\t\t\tdata-x=\"820\"
\t\t\t\t\t\t\tdata-y=\"170\"
\t\t\t\t\t\t\tdata-speed=\"1000\"
\t\t\t\t\t\t\tdata-start=\"1700\"
\t\t\t\t\t\t\tdata-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\tdata-endspeed=\"500\"
\t\t\t\t\t\t\tdata-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\tstyle=\"text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<strong><span class=\"text-color\">Join</span> SHP</strong>
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t<div class=\"tp-caption lfr skewtoright description text-right hidden-xs\"
\t\t\t\t\t\t\t data-x=\"450\"
\t\t\t\t\t\t\t data-y=\"290\"
\t\t\t\t\t\t\t data-speed=\"1000\"
\t\t\t\t\t\t\t data-start=\"1900\"
\t\t\t\t\t\t\t data-easing=\"Power4.easeOut\"
\t\t\t\t\t\t\t data-endspeed=\"500\"
\t\t\t\t\t\t\t data-endeasing=\"Power1.easeIn\"
\t\t\t\t\t\t\t style=\"max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;\">
\t\t\t\t\t\t\t<h4>More than 675 local healthcare providers have joined ";
        // line 110
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo "<br/><a href=\"http://www.scottsdalehealthpartners.com/\"><span style=\"background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;\">Click here</span></a> to login or to request access<br/>to the SHP physician member website</h4>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/slider/slide-3-back.jpg"), "html", null, true);
        echo "\" alt=\"\" data-bgfit=\"cover\" data-bgposition=\"center top\" data-bgrepeat=\"no-repeat\">
\t\t\t\t</li>
\t\t\t\t<!-- Slide Ends -->
\t\t\t</ul>
\t\t</div>
\t\t<hr class=\"no-margin\">
\t</section>
\t<!-- slider -->
";
        
        $__internal_16fca72ae44b2e22d20b373f7623d462ba9694d8976a814eeb5446ad27228c43->leave($__internal_16fca72ae44b2e22d20b373f7623d462ba9694d8976a814eeb5446ad27228c43_prof);

    }

    // line 125
    public function block_pe_special_features($context, array $blocks = array())
    {
        $__internal_4e904fe4401bc26b3bcec45104dcae5e60ca42a3811f526c6958acd9003a5cf5 = $this->env->getExtension("native_profiler");
        $__internal_4e904fe4401bc26b3bcec45104dcae5e60ca42a3811f526c6958acd9003a5cf5->enter($__internal_4e904fe4401bc26b3bcec45104dcae5e60ca42a3811f526c6958acd9003a5cf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_special_features"));

        // line 126
        echo "\t<div class=\"row special-feature\">
\t\t<!-- Special Feature Box 1 -->
\t\t<div class=\"col-xs-12 col-sm-3\">
\t\t\t<div class=\"s-feature-box text-center\">
\t\t\t\t<a href=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("our_physicians");
        echo "\">
\t\t\t\t\t<div class=\"mask-top\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-user-md\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Physician Directory</h4></div>
\t\t\t\t\t<div class=\"mask-bottom\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-user-md\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Physician Directory</h4>
\t\t\t\t\t\t<!-- Text -->
\t\t\t\t\t\t<p>Search more than 675 local physicians in the SHP network</p>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t\t<!-- Special Feature Box 2 -->
\t\t<div class=\"col-xs-12 col-sm-3\">
\t\t\t<div class=\"s-feature-box text-center\">
\t\t\t\t<a href=\"";
        // line 150
        echo $this->env->getExtension('routing')->getPath("hospitals");
        echo "\">
\t\t\t\t\t<div class=\"mask-top\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-hospital-o\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Hospitals</h4></div>
\t\t\t\t\t<div class=\"mask-bottom\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-hospital-o\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Hospitals</h4>
\t\t\t\t\t\t<!-- Text -->
\t\t\t\t\t\t<p>";
        // line 162
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " is aligned with 5 quality hospitals in the Phoenix area</p>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t\t<!-- Special Feature Box 3 -->
\t\t<div class=\"col-xs-12 col-sm-3\">
\t\t\t<div class=\"s-feature-box text-center\">
\t\t\t\t<a href=\"";
        // line 170
        echo $this->env->getExtension('routing')->getPath("insurance_plans");
        echo "\">
\t\t\t\t\t<div class=\"mask-top\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-heartbeat\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Insurance Plans</h4></div>
\t\t\t\t\t<div class=\"mask-bottom\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-heartbeat\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Insurance Plans</h4>
\t\t\t\t\t\t<!-- Text -->
\t\t\t\t\t\t<p>";
        // line 182
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " has contracts with major insurance companies</p>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t\t<!-- Special Feature Box 3 -->
\t\t<div class=\"col-xs-12 col-sm-3\">
\t\t\t<div class=\"s-feature-box text-center\">
\t\t\t\t<a href=\"https://scottsdalehealthpartners.com/\" target=\"_blank\">
\t\t\t\t\t<div class=\"mask-top\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-users\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Members</h4></div>
\t\t\t\t\t<div class=\"mask-bottom\">
\t\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t\t<i class=\"fa fa-users\"></i>
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h4>Members</h4>
\t\t\t\t\t\t<!-- Text -->
\t\t\t\t\t\t<p>Would you like to know more about joining the SHP physician network?</p>
\t\t\t\t\t\t<p><strong>Log in</strong> or <strong>request access</strong> to the SHP Physician Member website</p>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_4e904fe4401bc26b3bcec45104dcae5e60ca42a3811f526c6958acd9003a5cf5->leave($__internal_4e904fe4401bc26b3bcec45104dcae5e60ca42a3811f526c6958acd9003a5cf5_prof);

    }

    // line 213
    public function block_pe_who_we_are($context, array $blocks = array())
    {
        $__internal_4caab14c46651a8ba2dd777193176d0e60e85809d22654c35ea960d7a7ace327 = $this->env->getExtension("native_profiler");
        $__internal_4caab14c46651a8ba2dd777193176d0e60e85809d22654c35ea960d7a7ace327->enter($__internal_4caab14c46651a8ba2dd777193176d0e60e85809d22654c35ea960d7a7ace327_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_who_we_are"));

        // line 214
        echo "\t<div class=\"owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible\" data-animation=\"fadeInRight\" data-singleitem=\"true\" data-navigation=\"false\" data-autoplay=\"false\" data-pagination=\"true\">
\t\t<div class=\"item\">
\t\t\t<!-- Heading -->
\t\t\t<h2 class=\"entry-title\"><a href=\"#\">Our philosophy</a></h2>
\t\t\t<!-- Content -->
\t\t\t<div class=\"entry-content\">
\t\t\t\t<p>SHP is committed to transforming healthcare to provide coordinated, patient-centered care of the highest quality and value.</p>
\t\t\t\t<ul class=\"list-style small\">
\t\t\t\t\t<label>We believe this is achieved through:</label>
\t\t\t\t\t<li>Strong relationships between primary care providers and patients</li>
\t\t\t\t\t<li>Following evidence-based care guidelines</li>
\t\t\t\t\t<li>Providing care coordination services for patients to help them better navigate the complexity of healthcare and achieve their health goals</li>
\t\t\t\t\t<li>Collaborating, coordinating and communicating within a tightly integrated community of care providers</li>
\t\t\t\t\t<li>Obtaining, analyzing, and leveraging clinical and quality data for better health outcomes</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"item\">
\t\t\t<!-- Heading -->
\t\t\t<h2 class=\"entry-title\"><a href=\"#\">Our mission</a></h2>
\t\t\t<!-- Content -->
\t\t\t<div class=\"entry-content\">
\t\t\t\t<p>";
        // line 236
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " is a collaboration of medical professionals and a local healthcare system committed to providing high quality, coordinated and innovative care for the patients and families we serve.</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"item\">
\t\t\t<!-- Heading -->
\t\t\t<h2 class=\"entry-title\"><a href=\"#\">Our history</a></h2>
\t\t\t<!-- Content -->
\t\t\t<div class=\"entry-content\">
\t\t\t\t<p>";
        // line 244
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " was established in the summer of 2012 as a joint venture between Scottsdale Healthcare Hospitals (now HonorHealth) and Scottsdale Physician Organization.</p>
\t\t\t\t<p>SHP launched with 430 physicians, one insurance contract, and about 3,500 patients.</p>
\t\t\t\t<p>Today SHP has seven contracts with major insurance companies and now has over <strong>675</strong> physician members serving more than <strong></strong>30,000</strong> patients in the community.</p>
\t\t\t\t<p>SHP represents a unique collaboration between independent community physicians and a community hospital system where all parties share a common mission and vision to improve healthcare in the local community.</p>
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_4caab14c46651a8ba2dd777193176d0e60e85809d22654c35ea960d7a7ace327->leave($__internal_4caab14c46651a8ba2dd777193176d0e60e85809d22654c35ea960d7a7ace327_prof);

    }

    // line 255
    public function block_pe_patient_testimonials($context, array $blocks = array())
    {
        $__internal_9e0b619fa84cd005fde70330e954134ea8d81a48b8323310c544bdebd9263b29 = $this->env->getExtension("native_profiler");
        $__internal_9e0b619fa84cd005fde70330e954134ea8d81a48b8323310c544bdebd9263b29->enter($__internal_9e0b619fa84cd005fde70330e954134ea8d81a48b8323310c544bdebd9263b29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_patient_testimonials"));

        // line 256
        echo "\t<div class=\"owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible\" data-animation=\"fadeInRight\" data-singleitem=\"true\" data-navigation=\"false\" data-autoplay=\"7000\" data-pagination=\"true\" data-effect=\"backSlide\">
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tI appreciate how you have helped me, and as a Wounded Vet you have done more for me than the VA has in 10 years...I and the Army thank you for caring for soldiers like myself, who have loss trust in most of the medical profession. (Not you guys- you bring hope).
\t\t\t\t</blockquote>
\t\t\t\t";
        // line 263
        echo "\t\t\t\t";
        // line 264
        echo "\t\t\t\t";
        // line 265
        echo "\t\t\t\t";
        // line 266
        echo "\t\t\t\t";
        // line 267
        echo "\t\t\t\t";
        // line 268
        echo "\t\t\t\t";
        // line 269
        echo "\t\t\t</div>
\t\t\t";
        // line 271
        echo "\t\t\t\t";
        // line 272
        echo "\t\t\t\t";
        // line 273
        echo "\t\t\t\t";
        // line 274
        echo "\t\t\t\t";
        // line 275
        echo "\t\t\t\t";
        // line 276
        echo "\t\t\t\t\t<!-- Name -->
\t\t\t\t\t";
        // line 278
        echo "\t\t\t\t\t<!-- Company -->
\t\t\t\t\t";
        // line 280
        echo "\t\t\t\t";
        // line 281
        echo "\t\t\t";
        // line 282
        echo "\t\t</div>
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tI would like to thank you for your concern and information while my mom was in Scottsdale Healthcare Hospital. Your guidance to palliative care was the best choice. Your kindness and understanding meant a lot to me and my family. Thank you very much.
\t\t\t\t</blockquote>
\t\t\t\t";
        // line 289
        echo "\t\t\t\t";
        // line 290
        echo "\t\t\t\t";
        // line 291
        echo "\t\t\t\t";
        // line 292
        echo "\t\t\t\t";
        // line 293
        echo "\t\t\t\t";
        // line 294
        echo "\t\t\t\t";
        // line 295
        echo "\t\t\t</div>
\t\t\t";
        // line 297
        echo "\t\t\t\t";
        // line 298
        echo "\t\t\t\t";
        // line 299
        echo "\t\t\t\t";
        // line 300
        echo "\t\t\t\t";
        // line 301
        echo "\t\t\t\t";
        // line 302
        echo "\t\t\t\t\t<!-- Name -->
\t\t\t\t\t";
        // line 304
        echo "\t\t\t\t\t<!-- Company -->
\t\t\t\t\t";
        // line 306
        echo "\t\t\t\t";
        // line 307
        echo "\t\t\t";
        // line 308
        echo "\t\t</div>
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tYou have been very helpful in helping my mom agree to an assisted living.  She has agreed and I have found a place for her.  I was a little nervous about this experience but I wanted to call you and thank you so very, very, much for all of your help, your kindness, and your understanding.  Thank You  - Thank You
\t\t\t\t</blockquote>
\t\t\t\t";
        // line 315
        echo "\t\t\t\t";
        // line 316
        echo "\t\t\t\t";
        // line 317
        echo "\t\t\t\t";
        // line 318
        echo "\t\t\t\t";
        // line 319
        echo "\t\t\t\t";
        // line 320
        echo "\t\t\t\t";
        // line 321
        echo "\t\t\t</div>
\t\t\t";
        // line 323
        echo "\t\t\t\t";
        // line 324
        echo "\t\t\t\t";
        // line 325
        echo "\t\t\t\t";
        // line 326
        echo "\t\t\t\t";
        // line 327
        echo "\t\t\t\t";
        // line 328
        echo "\t\t\t\t\t<!-- Name -->
\t\t\t\t\t";
        // line 330
        echo "\t\t\t\t\t<!-- Company -->
\t\t\t\t\t";
        // line 332
        echo "\t\t\t\t";
        // line 333
        echo "\t\t\t";
        // line 334
        echo "\t\t</div>
\t</div>
";
        
        $__internal_9e0b619fa84cd005fde70330e954134ea8d81a48b8323310c544bdebd9263b29->leave($__internal_9e0b619fa84cd005fde70330e954134ea8d81a48b8323310c544bdebd9263b29_prof);

    }

    // line 340
    public function block_pe_physician_testimonials($context, array $blocks = array())
    {
        $__internal_9832dd7bbabd1f6437d7a4359bc5949e05cd97d9ba83bec90128dcffe853607c = $this->env->getExtension("native_profiler");
        $__internal_9832dd7bbabd1f6437d7a4359bc5949e05cd97d9ba83bec90128dcffe853607c->enter($__internal_9832dd7bbabd1f6437d7a4359bc5949e05cd97d9ba83bec90128dcffe853607c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_physician_testimonials"));

        // line 341
        echo "\t<div class=\"owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible\" data-animation=\"fadeInRight\" data-singleitem=\"true\" data-navigation=\"false\" data-autoplay=\"true\" data-pagination=\"true\" data-effect=\"backSlide\">
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.
\t\t\t\t</blockquote>
\t\t\t\t<div class=\"star-rating text-center\">
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star-half-o  text-color\"></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"client-details text-center\">
\t\t\t\t<div class=\"client-image\">
\t\t\t\t\t<!-- Image -->
\t\t\t\t\t<img class=\"img-circle\" src=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/grid/1.jpg"), "html", null, true);
        echo "\" width=\"80\" height=\"80\" alt=\"\">
\t\t\t\t</div>
\t\t\t\t<div class=\"client-details\">
\t\t\t\t\t<!-- Name -->
\t\t\t\t\t<strong class=\"text-color\">Dr. John Doe</strong>
\t\t\t\t\t<!-- Company -->
\t\t\t\t\t<span>Pediatric Surgeon, Honor Health</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.
\t\t\t\t</blockquote>
\t\t\t\t<div class=\"star-rating text-center\">
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star-half-o  text-color\"></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"client-details text-center\">
\t\t\t\t<div class=\"client-image\">
\t\t\t\t\t<!-- Image -->
\t\t\t\t\t<img class=\"img-circle\" src=\"";
        // line 384
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/grid/2.jpg"), "html", null, true);
        echo "\" width=\"80\" height=\"80\" alt=\"\">
\t\t\t\t</div>
\t\t\t\t<div class=\"client-details\">
\t\t\t\t\t<!-- Name -->
\t\t\t\t\t<strong class=\"text-color\">Dr. John Doe</strong>
\t\t\t\t\t<!-- Company -->
\t\t\t\t\t<span>Pediatric Surgeon, Honor Health</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"item\">
\t\t\t<div class=\"desc-border bottom-arrow\">
\t\t\t\t<blockquote class=\"small-text text-center\">
\t\t\t\t\tThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.
\t\t\t\t</blockquote>
\t\t\t\t<div class=\"star-rating text-center\">
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star  text-color\"></i>
\t\t\t\t\t<i class=\"fa fa-star-half-o  text-color\"></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"client-details text-center\">
\t\t\t\t<div class=\"client-image\">
\t\t\t\t\t<!-- Image -->
\t\t\t\t\t<img class=\"img-circle\" src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/grid/3.jpg"), "html", null, true);
        echo "\" width=\"80\" height=\"80\" alt=\"\">
\t\t\t\t</div>
\t\t\t\t<div class=\"client-details\">
\t\t\t\t\t<!-- Name -->
\t\t\t\t\t<strong class=\"text-color\">Dr. John Doe</strong>
\t\t\t\t\t<!-- Company -->
\t\t\t\t\t<span>Pediatric Surgeon, Honor Health</span>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_9832dd7bbabd1f6437d7a4359bc5949e05cd97d9ba83bec90128dcffe853607c->leave($__internal_9832dd7bbabd1f6437d7a4359bc5949e05cd97d9ba83bec90128dcffe853607c_prof);

    }

    // line 425
    public function block_pe_counters($context, array $blocks = array())
    {
        $__internal_75b301329aa0d7c530e7735cba0028157ca1e670cdde75f253a62a676ee0c77e = $this->env->getExtension("native_profiler");
        $__internal_75b301329aa0d7c530e7735cba0028157ca1e670cdde75f253a62a676ee0c77e->enter($__internal_75b301329aa0d7c530e7735cba0028157ca1e670cdde75f253a62a676ee0c77e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_counters"));

        // line 426
        echo "\t<section id=\"fun-factor\" class=\"page-section\">
\t\t<div class=\"image-bg content-in fixed\" data-background=\"";
        // line 427
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/bg/counters-back.jpg"), "html", null, true);
        echo "\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row text-center fact-counter white\">
\t\t\t\t<div class=\"col-sm-4 col-md-3 bottom-xs-pad-30\">
\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t<div class=\"count-number\" data-count=\"5\"><span class=\"counter\"></span></div>
\t\t\t\t\t<!-- Title -->
\t\t\t\t\t<h5>Hospitals</h5>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-md-3 bottom-xs-pad-30\">
\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t<div class=\"count-number\" data-count=\"675\"><span class=\"counter plus\"></span></div>
\t\t\t\t\t<!-- Title -->
\t\t\t\t\t<h5>Physicians</h5>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-md-3 bottom-xs-pad-30\">
\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t<div class=\"count-number\" data-count=\"390\"><span class=\"counter plus\"></span></div>
\t\t\t\t\t<!-- Title -->
\t\t\t\t\t<h5>Locations</h5>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-md-3 bottom-xs-pad-30\">
\t\t\t\t\t<!-- Icon -->
\t\t\t\t\t<div class=\"count-number\" data-count=\"59\"><span class=\"counter\"></span></div>
\t\t\t\t\t<!-- Title -->
\t\t\t\t\t<h5>Specialties</h5>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- fun-factor -->
";
        
        $__internal_75b301329aa0d7c530e7735cba0028157ca1e670cdde75f253a62a676ee0c77e->leave($__internal_75b301329aa0d7c530e7735cba0028157ca1e670cdde75f253a62a676ee0c77e_prof);

    }

    // line 462
    public function block_pe_google_map($context, array $blocks = array())
    {
        $__internal_7a218c7b4a2b79c762df7a1bdf9db5168b27568b3d5bd97ac9b65ef1e73f527f = $this->env->getExtension("native_profiler");
        $__internal_7a218c7b4a2b79c762df7a1bdf9db5168b27568b3d5bd97ac9b65ef1e73f527f->enter($__internal_7a218c7b4a2b79c762df7a1bdf9db5168b27568b3d5bd97ac9b65ef1e73f527f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_google_map"));

        // line 463
        echo "\t<section id=\"map\">
\t\t<div class=\"map-section\">
\t\t\t<div class=\"map-canvas\"
\t\t\t     data-zoom=\"15\"
\t\t\t     data-zoomcontrol=\"false\"
\t\t\t     data-lat=\"";
        // line 468
        echo twig_escape_filter($this->env, (isset($context["company_location_latitude"]) ? $context["company_location_latitude"] : $this->getContext($context, "company_location_latitude")), "html", null, true);
        echo "\"
\t\t\t     data-lng=\"";
        // line 469
        echo twig_escape_filter($this->env, (isset($context["company_location_longitude"]) ? $context["company_location_longitude"] : $this->getContext($context, "company_location_longitude")), "html", null, true);
        echo "\"
\t\t\t     data-type=\"roadmap\"
\t\t\t     data-hue=\"#8eb4e3\"
\t\t\t     data-title=\"";
        // line 472
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo "\"
\t\t\t     data-content=\"Contact: ";
        // line 473
        echo twig_escape_filter($this->env, (isset($context["company_phone"]) ? $context["company_phone"] : $this->getContext($context, "company_phone")), "html", null, true);
        echo "<br>
\t\t\t\t\t\t<a href='mailto:info@scottsdalehealthpartners.com'>info@scottsdalehealthpartners.com</a>\"
\t\t\t     style=\"height: 376px;\">
\t\t\t</div>
\t\t</div>
\t</section> <!-- map -->
";
        
        $__internal_7a218c7b4a2b79c762df7a1bdf9db5168b27568b3d5bd97ac9b65ef1e73f527f->leave($__internal_7a218c7b4a2b79c762df7a1bdf9db5168b27568b3d5bd97ac9b65ef1e73f527f_prof);

    }

    // line 481
    public function block_pe_contact_divider($context, array $blocks = array())
    {
        $__internal_6b5cbf979127f384c01971825926aa044364cb54136734c2bd503b85dcba43ce = $this->env->getExtension("native_profiler");
        $__internal_6b5cbf979127f384c01971825926aa044364cb54136734c2bd503b85dcba43ce->enter($__internal_6b5cbf979127f384c01971825926aa044364cb54136734c2bd503b85dcba43ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_contact_divider"));

        // line 482
        echo "\t<div id=\"get-quote\" class=\"bg-color get-a-quote black text-center\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\tNeed Help? <a class=\"black\" href=\"";
        // line 486
        echo $this->env->getExtension('routing')->getPath("contact");
        echo "\">Contact Us </a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_6b5cbf979127f384c01971825926aa044364cb54136734c2bd503b85dcba43ce->leave($__internal_6b5cbf979127f384c01971825926aa044364cb54136734c2bd503b85dcba43ce_prof);

    }

    // line 493
    public function block_pe_our_locations_map($context, array $blocks = array())
    {
        $__internal_ebe2a680e0b23e42a0de1be3fea86cdcb7b2a512f08510839b04025269f4c587 = $this->env->getExtension("native_profiler");
        $__internal_ebe2a680e0b23e42a0de1be3fea86cdcb7b2a512f08510839b04025269f4c587->enter($__internal_ebe2a680e0b23e42a0de1be3fea86cdcb7b2a512f08510839b04025269f4c587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_our_locations_map"));

        // line 494
        echo "\t<section id=\"our-locations\" class=\"page-section\">
\t\t<div class=\"image-bg content-in fixed\" data-background=\"";
        // line 495
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/bg/locations-map.jpg"), "html", null, true);
        echo "\"></div>
\t\t<div class=\"container\">
\t\t\t<div class=\"row text-center fact-counter white\">
\t\t\t\t<div class=\"col-sm-12 bottom-xs-pad-30\">
\t\t\t\t\t<div class=\"section-title invert-divider\">
\t\t\t\t\t\t<!-- Heading -->
\t\t\t\t\t\t<h2 class=\"title\">Hundreds of Physicians</h2>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 504
        echo "\t\t\t\t\t<p><a href=\"";
        echo $this->env->getExtension('routing')->getPath("our_physicians");
        echo "\" class=\"btn btn-success\">Locate A Physician Now</a></p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
";
        
        $__internal_ebe2a680e0b23e42a0de1be3fea86cdcb7b2a512f08510839b04025269f4c587->leave($__internal_ebe2a680e0b23e42a0de1be3fea86cdcb7b2a512f08510839b04025269f4c587_prof);

    }

    // line 511
    public function block_pe_footer_news($context, array $blocks = array())
    {
        $__internal_31bda103bede9db15c34fb777de38975b8ec551f05387ef5bdab0212f42b0a4c = $this->env->getExtension("native_profiler");
        $__internal_31bda103bede9db15c34fb777de38975b8ec551f05387ef5bdab0212f42b0a4c->enter($__internal_31bda103bede9db15c34fb777de38975b8ec551f05387ef5bdab0212f42b0a4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_footer_news"));

        // line 512
        echo "\t<div class=\"col-xs-12 col-sm-4 col-md-6 widget\">
\t\t<div class=\"widget-title\">
\t\t\t<!-- Title -->
\t\t\t<h3 class=\"title\">Latest News</h3>
\t\t</div>
\t\t<nav>
\t\t\t<ul class=\"footer-blog\">
\t\t\t\t<!-- List Items -->
\t\t\t\t<li><a href=\"";
        // line 520
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\">Shared Savings Program ACO Spotlight Newsletter</a></li>
\t\t\t\t<li><a href=\"";
        // line 521
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\">Journey to MSSP Management with Tailored ACO Web-Based Solution</a></li>
\t\t\t\t<li><a href=\"";
        // line 522
        echo $this->env->getExtension('routing')->getPath("news");
        echo "\">Going At Risk - A Vision for Connected Care</a></li>
\t\t\t</ul>
\t\t</nav>
\t</div>
";
        
        $__internal_31bda103bede9db15c34fb777de38975b8ec551f05387ef5bdab0212f42b0a4c->leave($__internal_31bda103bede9db15c34fb777de38975b8ec551f05387ef5bdab0212f42b0a4c_prof);

    }

    // line 528
    public function block_pe_googe_tracking($context, array $blocks = array())
    {
        $__internal_d55b0ed1a4317f337c3545d5fc684fb0b667facd5f7bd68c25f79195def1aca1 = $this->env->getExtension("native_profiler");
        $__internal_d55b0ed1a4317f337c3545d5fc684fb0b667facd5f7bd68c25f79195def1aca1->enter($__internal_d55b0ed1a4317f337c3545d5fc684fb0b667facd5f7bd68c25f79195def1aca1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_googe_tracking"));

        // line 529
        echo "\t<script>
\t\t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
\t\t\t\t\t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
\t\t\t\tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
\t\t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

\t\tga('create', 'UA-70325400-1', 'auto');
\t\tga('send', 'pageview');

\t</script>
";
        
        $__internal_d55b0ed1a4317f337c3545d5fc684fb0b667facd5f7bd68c25f79195def1aca1->leave($__internal_d55b0ed1a4317f337c3545d5fc684fb0b667facd5f7bd68c25f79195def1aca1_prof);

    }

    // line 541
    public function block_pe_sypfu($context, array $blocks = array())
    {
        $__internal_b9df61798693ba13dcb79c284d1fad6082524cf47eb53ed5b412af19577224db = $this->env->getExtension("native_profiler");
        $__internal_b9df61798693ba13dcb79c284d1fad6082524cf47eb53ed5b412af19577224db->enter($__internal_b9df61798693ba13dcb79c284d1fad6082524cf47eb53ed5b412af19577224db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pe_sypfu"));

        // line 542
        echo "\t";
        
        $__internal_b9df61798693ba13dcb79c284d1fad6082524cf47eb53ed5b412af19577224db->leave($__internal_b9df61798693ba13dcb79c284d1fad6082524cf47eb53ed5b412af19577224db_prof);

    }

    public function getTemplateName()
    {
        return "::page_elements.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  883 => 542,  877 => 541,  860 => 529,  854 => 528,  842 => 522,  838 => 521,  834 => 520,  824 => 512,  818 => 511,  804 => 504,  793 => 495,  790 => 494,  784 => 493,  771 => 486,  765 => 482,  759 => 481,  745 => 473,  741 => 472,  735 => 469,  731 => 468,  724 => 463,  718 => 462,  679 => 427,  676 => 426,  670 => 425,  651 => 410,  622 => 384,  593 => 358,  574 => 341,  568 => 340,  559 => 334,  557 => 333,  555 => 332,  552 => 330,  549 => 328,  547 => 327,  545 => 326,  543 => 325,  541 => 324,  539 => 323,  536 => 321,  534 => 320,  532 => 319,  530 => 318,  528 => 317,  526 => 316,  524 => 315,  516 => 308,  514 => 307,  512 => 306,  509 => 304,  506 => 302,  504 => 301,  502 => 300,  500 => 299,  498 => 298,  496 => 297,  493 => 295,  491 => 294,  489 => 293,  487 => 292,  485 => 291,  483 => 290,  481 => 289,  473 => 282,  471 => 281,  469 => 280,  466 => 278,  463 => 276,  461 => 275,  459 => 274,  457 => 273,  455 => 272,  453 => 271,  450 => 269,  448 => 268,  446 => 267,  444 => 266,  442 => 265,  440 => 264,  438 => 263,  430 => 256,  424 => 255,  409 => 244,  398 => 236,  374 => 214,  368 => 213,  333 => 182,  318 => 170,  307 => 162,  292 => 150,  269 => 130,  263 => 126,  257 => 125,  241 => 113,  235 => 110,  210 => 88,  192 => 73,  186 => 70,  156 => 43,  150 => 40,  125 => 18,  108 => 3,  102 => 2,  95 => 541,  92 => 540,  90 => 528,  87 => 527,  85 => 511,  82 => 510,  80 => 493,  77 => 492,  75 => 481,  72 => 480,  70 => 462,  66 => 459,  64 => 425,  60 => 422,  58 => 340,  54 => 337,  52 => 255,  48 => 252,  46 => 213,  42 => 210,  40 => 125,  36 => 122,  34 => 2,);
    }
}
/* {# HOME SLIDER #}*/
/* {% block pe_home_slider %}*/
/* 	<section class="slider">*/
/* 		<div class="tp-banner">*/
/* 			<ul>*/
/* 				<!-- Slide -->*/
/* 				<li data-delay="7000" class="bg-white" data-transition="fade" data-slotamount="7" data-start-height="300" data-masterspeed="2000">*/
/* 					<div class="elements">*/
/* 						<div class="tp-caption tp-resizeme lfb"*/
/* 							 data-x="450"*/
/* 							 data-y="95"*/
/* 							 data-speed="1000"*/
/* 							 data-start="2040"*/
/* 							 data-easing="Power4.easeOut"*/
/* 							 data-endspeed="500"*/
/* 							 data-endeasing="Power1.easeIn"*/
/* 							 style="z-index: 4; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<img src="{{ asset('bundles/site/images/sections/slider/slide-1.png') }}" alt="">*/
/* 						</div>*/
/* 						<h2 class="tp-caption lft skewtotop title bold"*/
/* 							data-x="12"*/
/* 							data-y="231"*/
/* 							data-speed="1000"*/
/* 							data-start="1700"*/
/* 							data-easing="Power4.easeOut"*/
/* 							data-endspeed="500"*/
/* 							data-endeasing="Power1.easeIn"*/
/* 							style="text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<strong><span class="text-color">Find</span> a <br>Physician</strong>*/
/* 						</h2>*/
/* 						<div class="tp-caption lfr skewtoright description text-left hidden-xs"*/
/* 							 data-x="12"*/
/* 							 data-y="400"*/
/* 							 data-speed="1000"*/
/* 							 data-start="1900"*/
/* 							 data-easing="Power4.easeOut"*/
/* 							 data-endspeed="500"*/
/* 							 data-endeasing="Power1.easeIn"*/
/* 							 style="max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<h4><a href="{{ path('our_physicians') }}"><span style="background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">Learn More</span></a></h4>*/
/* 						</div>*/
/* 					</div>*/
/* 					<img src="{{ asset('bundles/site/images/sections/slider/slide-1-back.jpg') }}" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">*/
/* 				</li>*/
/* 				<!-- Slide Ends -->*/
/* 				<!-- Slide -->*/
/* 				<li data-delay="7000" data-transition="fade" data-slotamount="7" data-start-height="300" data-masterspeed="2000">*/
/* 					<div class="elements">*/
/* 						<h2 class="tp-caption lft skewtotop title bold"*/
/* 							data-x="12"*/
/* 							data-y="151"*/
/* 							data-speed="1000"*/
/* 							data-start="1700"*/
/* 							data-easing="Power4.easeOut"*/
/* 							data-endspeed="500"*/
/* 							data-endeasing="Power1.easeIn"*/
/* 							style="text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<strong>Setting the standard <span class="text-color">for excellence</span><br/>in patient-centered care</strong>*/
/* 						</h2>*/
/* 						<div class="tp-caption lfr skewtoright description text-left hidden-xs"*/
/* 							 data-x="20"*/
/* 							 data-y="350"*/
/* 							 data-speed="1000"*/
/* 							 data-start="1900"*/
/* 							 data-easing="Power4.easeOut"*/
/* 							 data-endspeed="500"*/
/* 							 data-endeasing="Power1.easeIn"*/
/* 							 style="max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<h4>Our Care Coordinators and Transitional Care Managers are here to support you every step of the way<br/>*/
/* 							<a href="{{ path('care_management') }}"><span style="background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">Learn More</span></a></>*/
/* 						</div>*/
/* 					</div>*/
/* 					<img src="{{ asset('bundles/site/images/sections/slider/slide-2.jpg') }}" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">*/
/* 				</li>*/
/* 				<!-- Slide Ends -->*/
/* 				<!-- Slide -->*/
/* 				<li data-delay="7000" class="bg-white" data-transition="fade" data-slotamount="7" data-start-height="300" data-masterspeed="2000">*/
/* 					<div class="elements">*/
/* 						<div class="tp-caption tp-resizeme lfb"*/
/* 							 data-x="12"*/
/* 							 data-y="85"*/
/* 							 data-speed="1000"*/
/* 							 data-start="2040"*/
/* 							 data-easing="Power4.easeOut"*/
/* 							 data-endspeed="500"*/
/* 							 data-endeasing="Power1.easeIn"*/
/* 							 style="z-index: 4; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<img src="{{ asset('bundles/site/images/sections/slider/slide-3.png') }}" width="500" alt="">*/
/* 						</div>*/
/* 						<h2 class="tp-caption lft skewtotop title bold"*/
/* 							data-x="820"*/
/* 							data-y="170"*/
/* 							data-speed="1000"*/
/* 							data-start="1700"*/
/* 							data-easing="Power4.easeOut"*/
/* 							data-endspeed="500"*/
/* 							data-endeasing="Power1.easeIn"*/
/* 							style="text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<strong><span class="text-color">Join</span> SHP</strong>*/
/* 						</h2>*/
/* 						<div class="tp-caption lfr skewtoright description text-right hidden-xs"*/
/* 							 data-x="450"*/
/* 							 data-y="290"*/
/* 							 data-speed="1000"*/
/* 							 data-start="1900"*/
/* 							 data-easing="Power4.easeOut"*/
/* 							 data-endspeed="500"*/
/* 							 data-endeasing="Power1.easeIn"*/
/* 							 style="max-width: 600px; text-shadow: 0px 0px 20px #ffffff, 0px 0px 15px #ffffff, 0px 0px 10px #ffffff, 0px 0px 5px #ffffff;">*/
/* 							<h4>More than 675 local healthcare providers have joined {{ company_name }}<br/><a href="http://www.scottsdalehealthpartners.com/"><span style="background: #8EB4E6; padding: 5px 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">Click here</span></a> to login or to request access<br/>to the SHP physician member website</h4>*/
/* 						</div>*/
/* 					</div>*/
/* 					<img src="{{ asset('bundles/site/images/sections/slider/slide-3-back.jpg') }}" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">*/
/* 				</li>*/
/* 				<!-- Slide Ends -->*/
/* 			</ul>*/
/* 		</div>*/
/* 		<hr class="no-margin">*/
/* 	</section>*/
/* 	<!-- slider -->*/
/* {% endblock %}*/
/* */
/* */
/* {# SPEACIAL FEATURES #}*/
/* {% block pe_special_features %}*/
/* 	<div class="row special-feature">*/
/* 		<!-- Special Feature Box 1 -->*/
/* 		<div class="col-xs-12 col-sm-3">*/
/* 			<div class="s-feature-box text-center">*/
/* 				<a href="{{ path('our_physicians') }}">*/
/* 					<div class="mask-top">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-user-md"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Physician Directory</h4></div>*/
/* 					<div class="mask-bottom">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-user-md"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Physician Directory</h4>*/
/* 						<!-- Text -->*/
/* 						<p>Search more than 675 local physicians in the SHP network</p>*/
/* 					</div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- Special Feature Box 2 -->*/
/* 		<div class="col-xs-12 col-sm-3">*/
/* 			<div class="s-feature-box text-center">*/
/* 				<a href="{{ path('hospitals') }}">*/
/* 					<div class="mask-top">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-hospital-o"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Hospitals</h4></div>*/
/* 					<div class="mask-bottom">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-hospital-o"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Hospitals</h4>*/
/* 						<!-- Text -->*/
/* 						<p>{{ company_name }} is aligned with 5 quality hospitals in the Phoenix area</p>*/
/* 					</div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- Special Feature Box 3 -->*/
/* 		<div class="col-xs-12 col-sm-3">*/
/* 			<div class="s-feature-box text-center">*/
/* 				<a href="{{ path('insurance_plans') }}">*/
/* 					<div class="mask-top">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-heartbeat"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Insurance Plans</h4></div>*/
/* 					<div class="mask-bottom">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-heartbeat"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Insurance Plans</h4>*/
/* 						<!-- Text -->*/
/* 						<p>{{ company_name }} has contracts with major insurance companies</p>*/
/* 					</div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- Special Feature Box 3 -->*/
/* 		<div class="col-xs-12 col-sm-3">*/
/* 			<div class="s-feature-box text-center">*/
/* 				<a href="https://scottsdalehealthpartners.com/" target="_blank">*/
/* 					<div class="mask-top">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-users"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Members</h4></div>*/
/* 					<div class="mask-bottom">*/
/* 						<!-- Icon -->*/
/* 						<i class="fa fa-users"></i>*/
/* 						<!-- Title -->*/
/* 						<h4>Members</h4>*/
/* 						<!-- Text -->*/
/* 						<p>Would you like to know more about joining the SHP physician network?</p>*/
/* 						<p><strong>Log in</strong> or <strong>request access</strong> to the SHP Physician Member website</p>*/
/* 					</div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* */
/* {# WHO WE ARE #}*/
/* {% block pe_who_we_are %}*/
/* 	<div class="owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible" data-animation="fadeInRight" data-singleitem="true" data-navigation="false" data-autoplay="false" data-pagination="true">*/
/* 		<div class="item">*/
/* 			<!-- Heading -->*/
/* 			<h2 class="entry-title"><a href="#">Our philosophy</a></h2>*/
/* 			<!-- Content -->*/
/* 			<div class="entry-content">*/
/* 				<p>SHP is committed to transforming healthcare to provide coordinated, patient-centered care of the highest quality and value.</p>*/
/* 				<ul class="list-style small">*/
/* 					<label>We believe this is achieved through:</label>*/
/* 					<li>Strong relationships between primary care providers and patients</li>*/
/* 					<li>Following evidence-based care guidelines</li>*/
/* 					<li>Providing care coordination services for patients to help them better navigate the complexity of healthcare and achieve their health goals</li>*/
/* 					<li>Collaborating, coordinating and communicating within a tightly integrated community of care providers</li>*/
/* 					<li>Obtaining, analyzing, and leveraging clinical and quality data for better health outcomes</li>*/
/* 				</ul>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<!-- Heading -->*/
/* 			<h2 class="entry-title"><a href="#">Our mission</a></h2>*/
/* 			<!-- Content -->*/
/* 			<div class="entry-content">*/
/* 				<p>{{ company_name }} is a collaboration of medical professionals and a local healthcare system committed to providing high quality, coordinated and innovative care for the patients and families we serve.</p>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<!-- Heading -->*/
/* 			<h2 class="entry-title"><a href="#">Our history</a></h2>*/
/* 			<!-- Content -->*/
/* 			<div class="entry-content">*/
/* 				<p>{{ company_name }} was established in the summer of 2012 as a joint venture between Scottsdale Healthcare Hospitals (now HonorHealth) and Scottsdale Physician Organization.</p>*/
/* 				<p>SHP launched with 430 physicians, one insurance contract, and about 3,500 patients.</p>*/
/* 				<p>Today SHP has seven contracts with major insurance companies and now has over <strong>675</strong> physician members serving more than <strong></strong>30,000</strong> patients in the community.</p>*/
/* 				<p>SHP represents a unique collaboration between independent community physicians and a community hospital system where all parties share a common mission and vision to improve healthcare in the local community.</p>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* */
/* {# PATIENT TESTIMONIALS #}*/
/* {% block pe_patient_testimonials %}*/
/* 	<div class="owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible" data-animation="fadeInRight" data-singleitem="true" data-navigation="false" data-autoplay="7000" data-pagination="true" data-effect="backSlide">*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					I appreciate how you have helped me, and as a Wounded Vet you have done more for me than the VA has in 10 years...I and the Army thank you for caring for soldiers like myself, who have loss trust in most of the medical profession. (Not you guys- you bring hope).*/
/* 				</blockquote>*/
/* 				{#<div class="star-rating text-center">#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star-half-o  text-color"></i>#}*/
/* 				{#</div>#}*/
/* 			</div>*/
/* 			{#<div class="client-details text-center">#}*/
/* 				{#<div class="client-image">#}*/
/* 				{#<!-- Image -->#}*/
/* 				{#<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/1.jpg') }}" width="80" height="80" alt="">#}*/
/* 				{#</div>#}*/
/* 				{#<div class="client-details">#}*/
/* 					<!-- Name -->*/
/* 					{#<strong class="text-color">John</strong>#}*/
/* 					<!-- Company -->*/
/* 					{#<span>Pediatric Surgeon, Honor Health</span>#}*/
/* 				{#</div>#}*/
/* 			{#</div>#}*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					I would like to thank you for your concern and information while my mom was in Scottsdale Healthcare Hospital. Your guidance to palliative care was the best choice. Your kindness and understanding meant a lot to me and my family. Thank you very much.*/
/* 				</blockquote>*/
/* 				{#<div class="star-rating text-center">#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star-half-o  text-color"></i>#}*/
/* 				{#</div>#}*/
/* 			</div>*/
/* 			{#<div class="client-details text-center">#}*/
/* 				{#<div class="client-image">#}*/
/* 				{#<!-- Image -->#}*/
/* 				{#<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/2.jpg') }}" width="80" height="80" alt="">#}*/
/* 				{#</div>#}*/
/* 				{#<div class="client-details">#}*/
/* 					<!-- Name -->*/
/* 					{#<strong class="text-color">Anonymous</strong>#}*/
/* 					<!-- Company -->*/
/* 					{#<span>Pediatric Surgeon, Honor Health</span>#}*/
/* 				{#</div>#}*/
/* 			{#</div>#}*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					You have been very helpful in helping my mom agree to an assisted living.  She has agreed and I have found a place for her.  I was a little nervous about this experience but I wanted to call you and thank you so very, very, much for all of your help, your kindness, and your understanding.  Thank You  - Thank You*/
/* 				</blockquote>*/
/* 				{#<div class="star-rating text-center">#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star  text-color"></i>#}*/
/* 				{#<i class="fa fa-star-half-o  text-color"></i>#}*/
/* 				{#</div>#}*/
/* 			</div>*/
/* 			{#<div class="client-details text-center">#}*/
/* 				{#<div class="client-image">#}*/
/* 				{#<!-- Image -->#}*/
/* 				{#<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/3.jpg') }}" width="80" height="80" alt="">#}*/
/* 				{#</div>#}*/
/* 				{#<div class="client-details">#}*/
/* 					<!-- Name -->*/
/* 					{#<strong class="text-color">Anonymous</strong>#}*/
/* 					<!-- Company -->*/
/* 					{#<span>Pediatric Surgeon, Honor Health</span>#}*/
/* 				{#</div>#}*/
/* 			{#</div>#}*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* */
/* {# PHYSICIAN TESTIMONIALS #}*/
/* {% block pe_physician_testimonials %}*/
/* 	<div class="owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible" data-animation="fadeInRight" data-singleitem="true" data-navigation="false" data-autoplay="true" data-pagination="true" data-effect="backSlide">*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.*/
/* 				</blockquote>*/
/* 				<div class="star-rating text-center">*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star-half-o  text-color"></i>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="client-details text-center">*/
/* 				<div class="client-image">*/
/* 					<!-- Image -->*/
/* 					<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/1.jpg') }}" width="80" height="80" alt="">*/
/* 				</div>*/
/* 				<div class="client-details">*/
/* 					<!-- Name -->*/
/* 					<strong class="text-color">Dr. John Doe</strong>*/
/* 					<!-- Company -->*/
/* 					<span>Pediatric Surgeon, Honor Health</span>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.*/
/* 				</blockquote>*/
/* 				<div class="star-rating text-center">*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star-half-o  text-color"></i>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="client-details text-center">*/
/* 				<div class="client-image">*/
/* 					<!-- Image -->*/
/* 					<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/2.jpg') }}" width="80" height="80" alt="">*/
/* 				</div>*/
/* 				<div class="client-details">*/
/* 					<!-- Name -->*/
/* 					<strong class="text-color">Dr. John Doe</strong>*/
/* 					<!-- Company -->*/
/* 					<span>Pediatric Surgeon, Honor Health</span>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="item">*/
/* 			<div class="desc-border bottom-arrow">*/
/* 				<blockquote class="small-text text-center">*/
/* 					The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those intereste.*/
/* 				</blockquote>*/
/* 				<div class="star-rating text-center">*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star  text-color"></i>*/
/* 					<i class="fa fa-star-half-o  text-color"></i>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="client-details text-center">*/
/* 				<div class="client-image">*/
/* 					<!-- Image -->*/
/* 					<img class="img-circle" src="{{ asset('bundles/site/images/sections/grid/3.jpg') }}" width="80" height="80" alt="">*/
/* 				</div>*/
/* 				<div class="client-details">*/
/* 					<!-- Name -->*/
/* 					<strong class="text-color">Dr. John Doe</strong>*/
/* 					<!-- Company -->*/
/* 					<span>Pediatric Surgeon, Honor Health</span>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* */
/* {# COUNTERS #}*/
/* {%  block pe_counters %}*/
/* 	<section id="fun-factor" class="page-section">*/
/* 		<div class="image-bg content-in fixed" data-background="{{ asset('bundles/site/images/sections/bg/counters-back.jpg') }}"></div>*/
/* 		<div class="container">*/
/* 			<div class="row text-center fact-counter white">*/
/* 				<div class="col-sm-4 col-md-3 bottom-xs-pad-30">*/
/* 					<!-- Icon -->*/
/* 					<div class="count-number" data-count="5"><span class="counter"></span></div>*/
/* 					<!-- Title -->*/
/* 					<h5>Hospitals</h5>*/
/* 				</div>*/
/* 				<div class="col-sm-4 col-md-3 bottom-xs-pad-30">*/
/* 					<!-- Icon -->*/
/* 					<div class="count-number" data-count="675"><span class="counter plus"></span></div>*/
/* 					<!-- Title -->*/
/* 					<h5>Physicians</h5>*/
/* 				</div>*/
/* 				<div class="col-sm-4 col-md-3 bottom-xs-pad-30">*/
/* 					<!-- Icon -->*/
/* 					<div class="count-number" data-count="390"><span class="counter plus"></span></div>*/
/* 					<!-- Title -->*/
/* 					<h5>Locations</h5>*/
/* 				</div>*/
/* 				<div class="col-sm-4 col-md-3 bottom-xs-pad-30">*/
/* 					<!-- Icon -->*/
/* 					<div class="count-number" data-count="59"><span class="counter"></span></div>*/
/* 					<!-- Title -->*/
/* 					<h5>Specialties</h5>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</section>*/
/* 	<!-- fun-factor -->*/
/* {% endblock %}*/
/* */
/* */
/* {# GOOGLE MAP #}*/
/* {% block pe_google_map %}*/
/* 	<section id="map">*/
/* 		<div class="map-section">*/
/* 			<div class="map-canvas"*/
/* 			     data-zoom="15"*/
/* 			     data-zoomcontrol="false"*/
/* 			     data-lat="{{ company_location_latitude }}"*/
/* 			     data-lng="{{ company_location_longitude }}"*/
/* 			     data-type="roadmap"*/
/* 			     data-hue="#8eb4e3"*/
/* 			     data-title="{{ company_name }}"*/
/* 			     data-content="Contact: {{ company_phone }}<br>*/
/* 						<a href='mailto:info@scottsdalehealthpartners.com'>info@scottsdalehealthpartners.com</a>"*/
/* 			     style="height: 376px;">*/
/* 			</div>*/
/* 		</div>*/
/* 	</section> <!-- map -->*/
/* {% endblock %}*/
/* */
/* {% block pe_contact_divider %}*/
/* 	<div id="get-quote" class="bg-color get-a-quote black text-center">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<div class="col-md-12">*/
/* 					Need Help? <a class="black" href="{{ path('contact') }}">Contact Us </a>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* {% block pe_our_locations_map %}*/
/* 	<section id="our-locations" class="page-section">*/
/* 		<div class="image-bg content-in fixed" data-background="{{ asset('bundles/site/images/sections/bg/locations-map.jpg') }}"></div>*/
/* 		<div class="container">*/
/* 			<div class="row text-center fact-counter white">*/
/* 				<div class="col-sm-12 bottom-xs-pad-30">*/
/* 					<div class="section-title invert-divider">*/
/* 						<!-- Heading -->*/
/* 						<h2 class="title">Hundreds of Physicians</h2>*/
/* 					</div>*/
/* 					{#<p><a href="{{ path('physician_directory') }}" class="btn btn-success">Locate A Physician Now</a></p>#}*/
/* 					<p><a href="{{ path('our_physicians') }}" class="btn btn-success">Locate A Physician Now</a></p>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</section>*/
/* {% endblock %}*/
/* */
/* {% block pe_footer_news %}*/
/* 	<div class="col-xs-12 col-sm-4 col-md-6 widget">*/
/* 		<div class="widget-title">*/
/* 			<!-- Title -->*/
/* 			<h3 class="title">Latest News</h3>*/
/* 		</div>*/
/* 		<nav>*/
/* 			<ul class="footer-blog">*/
/* 				<!-- List Items -->*/
/* 				<li><a href="{{ path('news') }}">Shared Savings Program ACO Spotlight Newsletter</a></li>*/
/* 				<li><a href="{{ path('news') }}">Journey to MSSP Management with Tailored ACO Web-Based Solution</a></li>*/
/* 				<li><a href="{{ path('news') }}">Going At Risk - A Vision for Connected Care</a></li>*/
/* 			</ul>*/
/* 		</nav>*/
/* 	</div>*/
/* {% endblock %}*/
/* */
/* {% block pe_googe_tracking %}*/
/* 	<script>*/
/* 		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/* 					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/* 				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/* 		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');*/
/* */
/* 		ga('create', 'UA-70325400-1', 'auto');*/
/* 		ga('send', 'pageview');*/
/* */
/* 	</script>*/
/* {% endblock %}*/
/* */
/* {% block pe_sypfu %}*/
/* 	{#put spyfu code here#}*/
/* {% endblock %}*/
/* */
