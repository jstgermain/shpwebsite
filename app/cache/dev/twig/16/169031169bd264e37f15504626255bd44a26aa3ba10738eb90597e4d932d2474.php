<?php

/* SiteBundle:Home:home.html.twig */
class __TwigTemplate_427079c2acc3750234aefa23b0f4328bb22799905aef115518c1f52f05bec433 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("base.html.twig", "SiteBundle:Home:home.html.twig", 4);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_888d89c1952574e9e256bb93ec7280c485f7c37cdf986a471b1a18efe5feab13 = $this->env->getExtension("native_profiler");
        $__internal_888d89c1952574e9e256bb93ec7280c485f7c37cdf986a471b1a18efe5feab13->enter($__internal_888d89c1952574e9e256bb93ec7280c485f7c37cdf986a471b1a18efe5feab13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SiteBundle:Home:home.html.twig"));

        // line 1
        $context["meta_keywords"] = null;
        // line 2
        $context["meta_description"] = null;
        // line 3
        $context["meta_author"] = null;
        // line 4
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_888d89c1952574e9e256bb93ec7280c485f7c37cdf986a471b1a18efe5feab13->leave($__internal_888d89c1952574e9e256bb93ec7280c485f7c37cdf986a471b1a18efe5feab13_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_398027395c1492812f8086a5313027cdd13067e4a231ea87a58facd8f7558128 = $this->env->getExtension("native_profiler");
        $__internal_398027395c1492812f8086a5313027cdd13067e4a231ea87a58facd8f7558128->enter($__internal_398027395c1492812f8086a5313027cdd13067e4a231ea87a58facd8f7558128_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "\t";
        $this->displayBlock("pe_home_slider", $context, $blocks);
        echo "
\t<section id=\"about-us\" class=\"page-section\" data-animation=\"fadeInUp\">
\t\t<div class=\"container\">
\t\t\t<div class=\"section-title\">
\t\t\t\t<h1 class=\"title\">Who Is ";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo "?</h1>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 text-center\">
\t\t\t\t\t<!-- Text -->
\t\t\t\t\t<p class=\"title-description\">";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " is a physician-led Clinical Integration Network and Accountable Care Organization. We are dedicated to improving patient health and reducing healthcare spend through collaboration, coordination, and communication in the community.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        // line 19
        $this->displayBlock("pe_special_features", $context, $blocks);
        echo "
\t\t</div>
\t</section>
\t<!-- about-us -->
\t<section id=\"who-we-are\" class=\"page-section no-pad light-bg border-tb\">
\t\t<div class=\"container-fluid who-we-are\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6 pad-60\">
\t\t\t\t\t<div class=\"section-title text-left\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h2 class=\"title\">Who We Are</h2>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 31
        $this->displayBlock("pe_who_we_are", $context, $blocks);
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 no-pad text-center\">
\t\t\t\t\t<!-- Image -->
\t\t\t\t\t<div class=\"image-bg\" data-background=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/bg/who-we-are.jpg"), "html", null, true);
        echo "\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- who-we-are -->
\t<section id=\"additional\" class=\"page-section\">
\t\t<div class=\"container\">
\t\t\t<div class=\"section-title text-left\">
\t\t\t\t<!-- Heading -->
\t\t\t\t<h2 class=\"title\">Why Choose Us</h2>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4 text-center bottom-xs-pad-30\">
\t\t\t\t\t<!-- Image -->
\t\t\t\t\t";
        // line 51
        echo "\t\t\t\t\t";
        $this->displayBlock("pe_patient_testimonials", $context, $blocks);
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t";
        // line 55
        echo "\t\t\t\t\t";
        // line 56
        echo "\t\t\t\t\t\t";
        // line 57
        echo "\t\t\t\t\t\t";
        // line 58
        echo "\t\t\t\t\t";
        // line 59
        echo "\t\t\t\t\t<!-- Content -->
\t\t\t\t\t<p>";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " provides value-based patient care through nearly 300 practice locations and 5 local hospitals.</p>
\t\t\t\t\t<p>SHP physician practices accept multiple insurance plans offering convenience for patients including Commercial Insurance Plans, Medicare Advantage, and traditional Medicare insurance plans.</p>
\t\t\t\t\t<p>Patients who choose SHP providers have access to personalized care coordination, transitional care management, and optimized specialist referrals.</p>
\t\t\t\t\t<p>As a clinical integration network and an ACO, we strive to provide improved clinical quality, greater efficiency, improved collaboration, better access to care and long-term cost-control.</p>
\t\t\t\t\t<p>SHP physician members share in our vision to transform healthcare delivery through coordinated, patient-centered care of the highest quality and value.</p>
\t\t\t\t\t<!-- Features -->
\t\t\t\t\t<div class=\"row responsive-features top-pad-20\">
\t\t\t\t\t\t<!-- Features 1 -->
\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t<!-- Title And Content -->
\t\t\t\t\t\t\t<span class=\"icon-anchor text-color\"></span>
\t\t\t\t\t\t\t<h5>Care Management</h5>
\t\t\t\t\t\t\t<p>Industry leading Complex Care Coordination and Transitional Care Management programs</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- Features 1 -->
\t\t\t\t\t\t<!-- Features 2 -->
\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t<!-- Title And Content -->
\t\t\t\t\t\t\t<span class=\"icon-heart-fill text-color\"></span>
\t\t\t\t\t\t\t<h5>Positive Outcomes</h5>
\t\t\t\t\t\t\t<p>Proven track record of patient satisfaction, reduced readmissions, improved quality, and lowered cost</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- Features 2 -->
\t\t\t\t\t\t<!-- Features 3 -->
\t\t\t\t\t\t<div class=\"col-md-4\">
\t\t\t\t\t\t\t<!-- Title And Content -->
\t\t\t\t\t\t\t<span class=\"icon-user-md text-color\"></span>
\t\t\t\t\t\t\t<h5>Excellent Patient Care</h5>
\t\t\t\t\t\t\t<p>Caring physicians who take time to communicate and collaborate about patient care</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- Features 3 -->
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Features -->
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t</div>
\t</section>
\t<!-- additional -->
\t";
        // line 100
        echo "\t\t";
        // line 101
        echo "\t\t\t";
        // line 102
        echo "\t\t\t\t";
        // line 103
        echo "\t\t\t\t\t";
        // line 104
        echo "\t\t\t\t";
        // line 105
        echo "\t\t\t";
        // line 106
        echo "\t\t";
        // line 107
        echo "\t";
        // line 108
        echo "\t";
        $this->displayBlock("pe_counters", $context, $blocks);
        echo "
\t<section id=\"features\" class=\"page-section\">
\t\t<div class=\"container\">
\t\t\t<div class=\"section-title\">
\t\t\t\t<!-- Heading -->
\t\t\t\t<h2 class=\"title\">Our Results</h2>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"item-box col-sm-6 col-md-4\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h5 class=\"title\"><i class=\"fa fa-check fa-2x\"></i> Consistently Improved Quality Scores</h5>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-box col-sm-6 col-md-4\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h5 class=\"title\"><i class=\"fa fa-check fa-2x\"></i> High Patient Satisfaction Scores</h5>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"item-box col-sm-6 col-md-4\">
\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h5 class=\"title\"><i class=\"fa fa-check fa-2x\"></i> Up to 10% Annual Cost Improvement</h5>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</section>
\t<!-- Features -->

";
        
        $__internal_398027395c1492812f8086a5313027cdd13067e4a231ea87a58facd8f7558128->leave($__internal_398027395c1492812f8086a5313027cdd13067e4a231ea87a58facd8f7558128_prof);

    }

    public function getTemplateName()
    {
        return "SiteBundle:Home:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 108,  183 => 107,  181 => 106,  179 => 105,  177 => 104,  175 => 103,  173 => 102,  171 => 101,  169 => 100,  127 => 60,  124 => 59,  122 => 58,  120 => 57,  118 => 56,  116 => 55,  109 => 51,  91 => 35,  84 => 31,  69 => 19,  63 => 16,  55 => 11,  47 => 7,  41 => 6,  34 => 4,  32 => 3,  30 => 2,  28 => 1,  11 => 4,);
    }
}
/* {% set meta_keywords = null %}*/
/* {% set meta_description = null %}*/
/* {% set meta_author = null %}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* 	{{ block('pe_home_slider') }}*/
/* 	<section id="about-us" class="page-section" data-animation="fadeInUp">*/
/* 		<div class="container">*/
/* 			<div class="section-title">*/
/* 				<h1 class="title">Who Is {{ company_name }}?</h1>*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				<div class="col-md-12 text-center">*/
/* 					<!-- Text -->*/
/* 					<p class="title-description">{{ company_name }} is a physician-led Clinical Integration Network and Accountable Care Organization. We are dedicated to improving patient health and reducing healthcare spend through collaboration, coordination, and communication in the community.</p>*/
/* 				</div>*/
/* 			</div>*/
/* 			{{ block('pe_special_features') }}*/
/* 		</div>*/
/* 	</section>*/
/* 	<!-- about-us -->*/
/* 	<section id="who-we-are" class="page-section no-pad light-bg border-tb">*/
/* 		<div class="container-fluid who-we-are">*/
/* 			<div class="row">*/
/* 				<div class="col-md-6 pad-60">*/
/* 					<div class="section-title text-left">*/
/* 						<!-- Title -->*/
/* 						<h2 class="title">Who We Are</h2>*/
/* 					</div>*/
/* 					{{ block('pe_who_we_are') }}*/
/* 				</div>*/
/* 				<div class="col-md-6 no-pad text-center">*/
/* 					<!-- Image -->*/
/* 					<div class="image-bg" data-background="{{ asset('bundles/site/images/sections/bg/who-we-are.jpg') }}"></div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</section>*/
/* 	<!-- who-we-are -->*/
/* 	<section id="additional" class="page-section">*/
/* 		<div class="container">*/
/* 			<div class="section-title text-left">*/
/* 				<!-- Heading -->*/
/* 				<h2 class="title">Why Choose Us</h2>*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				<div class="col-md-4 text-center bottom-xs-pad-30">*/
/* 					<!-- Image -->*/
/* 					{#<img src="{{ asset('bundles/site/images/doctors.jpg') }}" width="280" height="440" alt="">#}*/
/* 					{{ block('pe_patient_testimonials') }}*/
/* 				</div>*/
/* 				<div class="col-md-8">*/
/* 					{# MOVING THIS TO SPAN BOTH COLUMNS FOR A MORE EVEN FEEL #}*/
/* 					{#<div class="section-title text-left">#}*/
/* 						{#<!-- Title -->#}*/
/* 						{#<h2 class="title">Why Choose Us.</h2>#}*/
/* 					{#</div>#}*/
/* 					<!-- Content -->*/
/* 					<p>{{ company_name }} provides value-based patient care through nearly 300 practice locations and 5 local hospitals.</p>*/
/* 					<p>SHP physician practices accept multiple insurance plans offering convenience for patients including Commercial Insurance Plans, Medicare Advantage, and traditional Medicare insurance plans.</p>*/
/* 					<p>Patients who choose SHP providers have access to personalized care coordination, transitional care management, and optimized specialist referrals.</p>*/
/* 					<p>As a clinical integration network and an ACO, we strive to provide improved clinical quality, greater efficiency, improved collaboration, better access to care and long-term cost-control.</p>*/
/* 					<p>SHP physician members share in our vision to transform healthcare delivery through coordinated, patient-centered care of the highest quality and value.</p>*/
/* 					<!-- Features -->*/
/* 					<div class="row responsive-features top-pad-20">*/
/* 						<!-- Features 1 -->*/
/* 						<div class="col-md-4">*/
/* 							<!-- Title And Content -->*/
/* 							<span class="icon-anchor text-color"></span>*/
/* 							<h5>Care Management</h5>*/
/* 							<p>Industry leading Complex Care Coordination and Transitional Care Management programs</p>*/
/* 						</div>*/
/* 						<!-- Features 1 -->*/
/* 						<!-- Features 2 -->*/
/* 						<div class="col-md-4">*/
/* 							<!-- Title And Content -->*/
/* 							<span class="icon-heart-fill text-color"></span>*/
/* 							<h5>Positive Outcomes</h5>*/
/* 							<p>Proven track record of patient satisfaction, reduced readmissions, improved quality, and lowered cost</p>*/
/* 						</div>*/
/* 						<!-- Features 2 -->*/
/* 						<!-- Features 3 -->*/
/* 						<div class="col-md-4">*/
/* 							<!-- Title And Content -->*/
/* 							<span class="icon-user-md text-color"></span>*/
/* 							<h5>Excellent Patient Care</h5>*/
/* 							<p>Caring physicians who take time to communicate and collaborate about patient care</p>*/
/* 						</div>*/
/* 						<!-- Features 3 -->*/
/* 					</div>*/
/* 					<!-- Features -->*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="clearfix"></div>*/
/* 		</div>*/
/* 	</section>*/
/* 	<!-- additional -->*/
/* 	{#<section id="testimonials" class="page-section light-bg">#}*/
/* 		{#<div class="container">#}*/
/* 			{#<div class="row">#}*/
/* 				{#<div class="col-sm-12 testimonails">#}*/
/* 					{#{{ block('pe_testimonials') }}#}*/
/* 				{#</div>#}*/
/* 			{#</div>#}*/
/* 		{#</div>#}*/
/* 	{#</section>#}*/
/* 	{{ block('pe_counters') }}*/
/* 	<section id="features" class="page-section">*/
/* 		<div class="container">*/
/* 			<div class="section-title">*/
/* 				<!-- Heading -->*/
/* 				<h2 class="title">Our Results</h2>*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				<div class="item-box col-sm-6 col-md-4">*/
/* 					<a href="#">*/
/* 						<!-- Title -->*/
/* 						<h5 class="title"><i class="fa fa-check fa-2x"></i> Consistently Improved Quality Scores</h5>*/
/* 					</a>*/
/* 				</div>*/
/* 				<div class="item-box col-sm-6 col-md-4">*/
/* 					<a href="#">*/
/* 						<!-- Title -->*/
/* 						<h5 class="title"><i class="fa fa-check fa-2x"></i> High Patient Satisfaction Scores</h5>*/
/* 					</a>*/
/* 				</div>*/
/* 				<div class="item-box col-sm-6 col-md-4">*/
/* 					<a href="#">*/
/* 						<!-- Title -->*/
/* 						<h5 class="title"><i class="fa fa-check fa-2x"></i> Up to 10% Annual Cost Improvement</h5>*/
/* 					</a>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</section>*/
/* 	<!-- Features -->*/
/* */
/* {% endblock %}*/
