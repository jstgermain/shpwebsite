<?php

/* ::footer.html.twig */
class __TwigTemplate_05bb18a20c545450dc92a9dba9178d7dc262c77fbe29c6fff25e56fab175cb73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99764c8a3e83c19fa0d19b05c06ca8fa6e41c6fa3372f42344b4a5291cc06d32 = $this->env->getExtension("native_profiler");
        $__internal_99764c8a3e83c19fa0d19b05c06ca8fa6e41c6fa3372f42344b4a5291cc06d32->enter($__internal_99764c8a3e83c19fa0d19b05c06ca8fa6e41c6fa3372f42344b4a5291cc06d32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        // line 1
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_99764c8a3e83c19fa0d19b05c06ca8fa6e41c6fa3372f42344b4a5291cc06d32->leave($__internal_99764c8a3e83c19fa0d19b05c06ca8fa6e41c6fa3372f42344b4a5291cc06d32_prof);

    }

    public function block_footer($context, array $blocks = array())
    {
        $__internal_cbef60c6b22afa76f0b32e1ac1da0276632aac5ec7621d5e3ec21e5d1331b777 = $this->env->getExtension("native_profiler");
        $__internal_cbef60c6b22afa76f0b32e1ac1da0276632aac5ec7621d5e3ec21e5d1331b777->enter($__internal_cbef60c6b22afa76f0b32e1ac1da0276632aac5ec7621d5e3ec21e5d1331b777_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 2
        echo "<footer id=\"footer\">
\t<div class=\"footer-widget\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12 col-sm-4 col-md-3 widget bottom-xs-pad-20\">
\t\t\t\t\t<div class=\"widget-title\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h3 class=\"title\">About Us</h3>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Text -->
\t\t\t\t\t";
        // line 13
        echo "\t\t\t\t\t<!-- Address -->
\t\t\t\t\t<p><strong>Office:</strong> ";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo "<br> ";
        echo twig_escape_filter($this->env, (isset($context["company_address1"]) ? $context["company_address1"] : $this->getContext($context, "company_address1")), "html", null, true);
        if (((isset($context["company_address2"]) ? $context["company_address2"] : $this->getContext($context, "company_address2")) != "")) {
            echo ", ";
            echo twig_escape_filter($this->env, (isset($context["company_address2"]) ? $context["company_address2"] : $this->getContext($context, "company_address2")), "html", null, true);
        }
        echo "<br/>";
        echo twig_escape_filter($this->env, (isset($context["company_city"]) ? $context["company_city"] : $this->getContext($context, "company_city")), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, (isset($context["company_state"]) ? $context["company_state"] : $this->getContext($context, "company_state")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["company_zip_code"]) ? $context["company_zip_code"] : $this->getContext($context, "company_zip_code")), "html", null, true);
        echo "</p>
\t\t\t\t\t<!-- Phone -->
\t\t\t\t\t<p><strong>Call Us:</strong> ";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["company_phone"]) ? $context["company_phone"] : $this->getContext($context, "company_phone")), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><strong>Follow Us:</strong> <span class=\"social-icon gray-bg icons-circle i-2x\"><a href=\"https://www.linkedin.com/company/scottsdale-health-partners\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></span></p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-12 col-sm-4 col-md-3 widget bottom-xs-pad-20\">
\t\t\t\t\t<div class=\"widget-title\">
\t\t\t\t\t\t<!-- Title -->
\t\t\t\t\t\t<h3 class=\"title\">Need Help?</h3>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 24
        echo $this->env->getExtension('knp_menu')->render("SiteBundle:Builder:footerMenu");
        echo "
\t\t\t\t</div>
\t\t\t\t";
        // line 26
        $this->displayBlock("pe_footer_news", $context, $blocks);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- footer-top -->
\t<div class=\"copyright\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<!-- Copyrights -->
\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-6 line-height-3x\">
\t\t\t\t\tCopyright &copy; ";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo ", &reg; ";
        echo twig_escape_filter($this->env, (isset($context["company_founded"]) ? $context["company_founded"] : $this->getContext($context, "company_founded")), "html", null, true);
        if ((twig_date_format_filter($this->env, "now", "Y") != (isset($context["company_founded"]) ? $context["company_founded"] : $this->getContext($context, "company_founded")))) {
            echo " - ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        }
        // line 37
        echo "\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-6 col-sm-6 col-md-6 text-right page-scroll gray-bg icons-circle i-3x\">
\t\t\t\t\t<!-- Goto Top -->
\t\t\t\t\t<a href=\"#page\">
\t\t\t\t\t\t<i class=\"glyphicon glyphicon-arrow-up no-margin\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- footer-bottom -->


</footer>
<!-- footer -->
";
        
        $__internal_cbef60c6b22afa76f0b32e1ac1da0276632aac5ec7621d5e3ec21e5d1331b777->leave($__internal_cbef60c6b22afa76f0b32e1ac1da0276632aac5ec7621d5e3ec21e5d1331b777_prof);

    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  104 => 37,  96 => 36,  83 => 26,  78 => 24,  67 => 16,  50 => 14,  47 => 13,  35 => 2,  23 => 1,);
    }
}
/* {% block footer %}*/
/* <footer id="footer">*/
/* 	<div class="footer-widget">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<div class="col-xs-12 col-sm-4 col-md-3 widget bottom-xs-pad-20">*/
/* 					<div class="widget-title">*/
/* 						<!-- Title -->*/
/* 						<h3 class="title">About Us</h3>*/
/* 					</div>*/
/* 					<!-- Text -->*/
/* 					{#<p> We like to provide great site with complete features what you want to impletement in your business!</p>#}*/
/* 					<!-- Address -->*/
/* 					<p><strong>Office:</strong> {{ company_name }}<br> {{ company_address1 }}{% if company_address2 != '' %}, {{ company_address2 }}{% endif %}<br/>{{ company_city }}, {{ company_state }} {{ company_zip_code }}</p>*/
/* 					<!-- Phone -->*/
/* 					<p><strong>Call Us:</strong> {{ company_phone }}</p>*/
/* 					<p><strong>Follow Us:</strong> <span class="social-icon gray-bg icons-circle i-2x"><a href="https://www.linkedin.com/company/scottsdale-health-partners" target="_blank"><i class="fa fa-linkedin"></i></a></span></p>*/
/* 				</div>*/
/* 				<div class="col-xs-12 col-sm-4 col-md-3 widget bottom-xs-pad-20">*/
/* 					<div class="widget-title">*/
/* 						<!-- Title -->*/
/* 						<h3 class="title">Need Help?</h3>*/
/* 					</div>*/
/* 					{{ knp_menu_render("SiteBundle:Builder:footerMenu") }}*/
/* 				</div>*/
/* 				{{ block('pe_footer_news') }}*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- footer-top -->*/
/* 	<div class="copyright">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<!-- Copyrights -->*/
/* 				<div class="col-xs-12 col-sm-6 col-md-6 line-height-3x">*/
/* 					Copyright &copy; {{ company_name }}, &reg; {{ company_founded }}{% if "now"|date("Y") != company_founded %} - {{ "now"|date("Y") }}{% endif %}*/
/* 				</div>*/
/* 				<div class="col-xs-6 col-sm-6 col-md-6 text-right page-scroll gray-bg icons-circle i-3x">*/
/* 					<!-- Goto Top -->*/
/* 					<a href="#page">*/
/* 						<i class="glyphicon glyphicon-arrow-up no-margin"></i>*/
/* 					</a>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- footer-bottom -->*/
/* */
/* */
/* </footer>*/
/* <!-- footer -->*/
/* {% endblock %}*/
