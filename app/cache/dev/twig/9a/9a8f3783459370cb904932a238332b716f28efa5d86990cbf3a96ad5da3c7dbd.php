<?php

/* ::navigation.html.twig */
class __TwigTemplate_e5295fb748524a9ea7a2852c16fe18982432100cfb34ee78ed6f8ae8894ebc9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main_navigation' => array($this, 'block_main_navigation'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0caf53dd14455c3f4534ec8cda88982a8c198bc706db7eabfc80b1e214754118 = $this->env->getExtension("native_profiler");
        $__internal_0caf53dd14455c3f4534ec8cda88982a8c198bc706db7eabfc80b1e214754118->enter($__internal_0caf53dd14455c3f4534ec8cda88982a8c198bc706db7eabfc80b1e214754118_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::navigation.html.twig"));

        // line 1
        $this->displayBlock('main_navigation', $context, $blocks);
        
        $__internal_0caf53dd14455c3f4534ec8cda88982a8c198bc706db7eabfc80b1e214754118->leave($__internal_0caf53dd14455c3f4534ec8cda88982a8c198bc706db7eabfc80b1e214754118_prof);

    }

    public function block_main_navigation($context, array $blocks = array())
    {
        $__internal_78100f72b9d848b77e7e3fce7e425d7243d8499a328413aa641f6d2de82161a4 = $this->env->getExtension("native_profiler");
        $__internal_78100f72b9d848b77e7e3fce7e425d7243d8499a328413aa641f6d2de82161a4->enter($__internal_78100f72b9d848b77e7e3fce7e425d7243d8499a328413aa641f6d2de82161a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_navigation"));

        // line 2
        echo "\t<!-- Sticky Navbar -->
\t<header id=\"sticker\" class=\"sticky-navigation\">
\t\t<!-- Sticky Menu -->
\t\t<div class=\"sticky-menu relative\">
\t\t\t<!-- navbar -->
\t\t\t<div class=\"navbar navbar-default navbar-bg-light\" role=\"navigation\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<div class=\"navbar-header\">
\t\t\t\t\t\t\t\t<!-- Button For Responsive toggle -->
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span></button>
\t\t\t\t\t\t\t\t<!-- Logo -->

\t\t\t\t\t\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">
\t\t\t\t\t\t\t\t\t<img class=\"site_logo\" alt=\"Site Logo\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/shp-logo.png"), "html", null, true);
        echo "\" />
\t\t\t\t\t\t\t\t</a></div>
\t\t\t\t\t\t\t<!-- Navbar Collapse -->
\t\t\t\t\t\t\t<div class=\"navbar-collapse collapse\">

\t\t\t\t\t\t\t\t<!-- nav -->
\t\t\t\t\t\t\t\t";
        // line 27
        echo $this->env->getExtension('knp_menu')->render("SiteBundle:Builder:mainMenu", array("allow_safe_labels" => true));
        echo "

\t\t\t\t\t\t\t\t<!-- Right nav -->
\t\t\t\t\t\t\t\t<!-- Header Contact Content -->
\t\t\t\t\t\t\t\t<div class=\"bg-white hide-show-content no-display header-contact-content\">
\t\t\t\t\t\t\t\t\t<p class=\"vertically-absolute-middle\">Call Us
\t\t\t\t\t\t\t\t\t\t<strong>";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["company_phone"]) ? $context["company_phone"] : $this->getContext($context, "company_phone")), "html", null, true);
        echo "</strong></p>
\t\t\t\t\t\t\t\t\t<button class=\"close\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Header Contact Content -->
\t\t\t\t\t\t\t\t<!-- Header Search Content -->
\t\t\t\t\t\t\t\t<div class=\"bg-white hide-show-content no-display header-search-content\">
\t\t\t\t\t\t\t\t\t<form role=\"search\" class=\"navbar-form vertically-absolute-middle\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" placeholder=\"Enter your text &amp; Search Here\" class=\"form-control\" id=\"s\" name=\"s\" value=\"\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t<button class=\"close\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Header Search Content -->
\t\t\t\t\t\t\t\t<!-- Header Share Content -->
\t\t\t\t\t\t\t\t<div class=\"bg-white hide-show-content no-display header-share-content\">
\t\t\t\t\t\t\t\t\t<div class=\"vertically-absolute-middle social-icon gray-bg icons-circle i-3x\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-facebook\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-twitter\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pinterest\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-google\"></i>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-linkedin\"></i>
\t\t\t\t\t\t\t\t\t\t</a></div>
\t\t\t\t\t\t\t\t\t<button class=\"close\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- Header Share Content -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- /.navbar-collapse -->
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- /.col-md-12 -->
\t\t\t\t\t</div>
\t\t\t\t\t<!-- /.row -->
\t\t\t\t</div>
\t\t\t\t<!-- /.container -->
\t\t\t</div>
\t\t\t<!-- navbar -->
\t\t</div>
\t\t<!-- Sticky Menu -->
\t</header>
\t<!-- Sticky Navbar -->
";
        
        $__internal_78100f72b9d848b77e7e3fce7e425d7243d8499a328413aa641f6d2de82161a4->leave($__internal_78100f72b9d848b77e7e3fce7e425d7243d8499a328413aa641f6d2de82161a4_prof);

    }

    public function getTemplateName()
    {
        return "::navigation.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  77 => 33,  68 => 27,  59 => 21,  55 => 20,  35 => 2,  23 => 1,);
    }
}
/* {% block main_navigation %}*/
/* 	<!-- Sticky Navbar -->*/
/* 	<header id="sticker" class="sticky-navigation">*/
/* 		<!-- Sticky Menu -->*/
/* 		<div class="sticky-menu relative">*/
/* 			<!-- navbar -->*/
/* 			<div class="navbar navbar-default navbar-bg-light" role="navigation">*/
/* 				<div class="container">*/
/* 					<div class="row">*/
/* 						<div class="col-md-12">*/
/* 							<div class="navbar-header">*/
/* 								<!-- Button For Responsive toggle -->*/
/* 								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/* 									<span class="sr-only">Toggle navigation</span>*/
/* 									<span class="icon-bar"></span>*/
/* 									<span class="icon-bar"></span>*/
/* 									<span class="icon-bar"></span></button>*/
/* 								<!-- Logo -->*/
/* */
/* 								<a class="navbar-brand" href="{{ path('homepage') }}">*/
/* 									<img class="site_logo" alt="Site Logo" src="{{ asset('bundles/site/images/shp-logo.png') }}" />*/
/* 								</a></div>*/
/* 							<!-- Navbar Collapse -->*/
/* 							<div class="navbar-collapse collapse">*/
/* */
/* 								<!-- nav -->*/
/* 								{{ knp_menu_render("SiteBundle:Builder:mainMenu", {'allow_safe_labels': true}) | raw }}*/
/* */
/* 								<!-- Right nav -->*/
/* 								<!-- Header Contact Content -->*/
/* 								<div class="bg-white hide-show-content no-display header-contact-content">*/
/* 									<p class="vertically-absolute-middle">Call Us*/
/* 										<strong>{{ company_phone }}</strong></p>*/
/* 									<button class="close">*/
/* 										<i class="fa fa-times"></i>*/
/* 									</button>*/
/* 								</div>*/
/* 								<!-- Header Contact Content -->*/
/* 								<!-- Header Search Content -->*/
/* 								<div class="bg-white hide-show-content no-display header-search-content">*/
/* 									<form role="search" class="navbar-form vertically-absolute-middle">*/
/* 										<div class="form-group">*/
/* 											<input type="text" placeholder="Enter your text &amp; Search Here" class="form-control" id="s" name="s" value="" />*/
/* 										</div>*/
/* 									</form>*/
/* 									<button class="close">*/
/* 										<i class="fa fa-times"></i>*/
/* 									</button>*/
/* 								</div>*/
/* 								<!-- Header Search Content -->*/
/* 								<!-- Header Share Content -->*/
/* 								<div class="bg-white hide-show-content no-display header-share-content">*/
/* 									<div class="vertically-absolute-middle social-icon gray-bg icons-circle i-3x">*/
/* 										<a href="#">*/
/* 											<i class="fa fa-facebook"></i>*/
/* 										</a>*/
/* 										<a href="#">*/
/* 											<i class="fa fa-twitter"></i>*/
/* 										</a>*/
/* 										<a href="#">*/
/* 											<i class="fa fa-pinterest"></i>*/
/* 										</a>*/
/* 										<a href="#">*/
/* 											<i class="fa fa-google"></i>*/
/* 										</a>*/
/* 										<a href="#">*/
/* 											<i class="fa fa-linkedin"></i>*/
/* 										</a></div>*/
/* 									<button class="close">*/
/* 										<i class="fa fa-times"></i>*/
/* 									</button>*/
/* 								</div>*/
/* 								<!-- Header Share Content -->*/
/* 							</div>*/
/* 							<!-- /.navbar-collapse -->*/
/* 						</div>*/
/* 						<!-- /.col-md-12 -->*/
/* 					</div>*/
/* 					<!-- /.row -->*/
/* 				</div>*/
/* 				<!-- /.container -->*/
/* 			</div>*/
/* 			<!-- navbar -->*/
/* 		</div>*/
/* 		<!-- Sticky Menu -->*/
/* 	</header>*/
/* 	<!-- Sticky Navbar -->*/
/* {% endblock %}*/
