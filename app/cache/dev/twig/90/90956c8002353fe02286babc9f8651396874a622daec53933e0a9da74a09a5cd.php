<?php

/* SiteBundle:About:executiveTeam.html.twig */
class __TwigTemplate_543fd76e138ba7b05fd815c2e530493a5367163b1f82a6456fe2936ac0b1f0c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 5
        $this->parent = $this->loadTemplate("::base.html.twig", "SiteBundle:About:executiveTeam.html.twig", 5);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d442ffc7a20aafc856b5378489732c6aabf66d309a07a6098e5581ffea03d481 = $this->env->getExtension("native_profiler");
        $__internal_d442ffc7a20aafc856b5378489732c6aabf66d309a07a6098e5581ffea03d481->enter($__internal_d442ffc7a20aafc856b5378489732c6aabf66d309a07a6098e5581ffea03d481_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SiteBundle:About:executiveTeam.html.twig"));

        // line 1
        $context["title"] = "Executive Team";
        // line 2
        $context["meta_keywords"] = null;
        // line 3
        $context["meta_description"] = null;
        // line 4
        $context["meta_author"] = null;
        // line 5
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d442ffc7a20aafc856b5378489732c6aabf66d309a07a6098e5581ffea03d481->leave($__internal_d442ffc7a20aafc856b5378489732c6aabf66d309a07a6098e5581ffea03d481_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_2394d2ae2135eab30abc59ff0d1f4a22d8e420d8984e2598d75448961729713e = $this->env->getExtension("native_profiler");
        $__internal_2394d2ae2135eab30abc59ff0d1f4a22d8e420d8984e2598d75448961729713e->enter($__internal_2394d2ae2135eab30abc59ff0d1f4a22d8e420d8984e2598d75448961729713e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "SiteBundle:About:executiveTeams";
        
        $__internal_2394d2ae2135eab30abc59ff0d1f4a22d8e420d8984e2598d75448961729713e->leave($__internal_2394d2ae2135eab30abc59ff0d1f4a22d8e420d8984e2598d75448961729713e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_19a518478b58420a1e6c036081b74ddb78ce451f2ee16342e2fbb5900351e1fc = $this->env->getExtension("native_profiler");
        $__internal_19a518478b58420a1e6c036081b74ddb78ce451f2ee16342e2fbb5900351e1fc->enter($__internal_19a518478b58420a1e6c036081b74ddb78ce451f2ee16342e2fbb5900351e1fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "\t<div class=\"page-header\">
\t\t<div class=\"container\">
\t\t\t<h1 class=\"title\">";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "</h1>
\t\t</div>
\t</div>
\t<!-- page-header -->
\t<section id=\"executive-team\" class=\"page-section\">
\t\t<div class=\"container text-center\">
\t\t\t<div class=\"col-sm-12 col-md-12\">
\t\t\t\t<div class=\"section-title\">
\t\t\t\t\t<!-- Heading -->
\t\t\t\t\t<h2 class=\"title\">Meet the ";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "</h2>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container\">
\t\t\t\t\t<div class=\"thumb-info\">
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t<!-- Image -->
\t\t\t\t\t\t\t<img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/about/tiffany-nelson-thumb.jpg"), "html", null, true);
        echo "\" alt=\"\" title=\"\" width=\"270\" height=\"270\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"description-container\">
\t\t\t\t\t\t\t<div class=\"description\">
\t\t\t\t\t\t\t\t<!-- Name -->
\t\t\t\t\t\t\t\t<h2 class=\"name\">Tiffany Nelson</h2>
\t\t\t\t\t\t\t\t<!-- Designation -->
\t\t\t\t\t\t\t\t<p class=\"role\">MD</p>
\t\t\t\t\t\t\t\t<p><a href=\"#tiffany-nelson\" data-toggle=\"modal\" data-target=\"#tiffany-nelson\"><i class=\"fa fa-info-circle fa-2x white\"></i></a></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<h6>Chief Strategy Officer</h6>
\t\t\t\t</div>
\t\t\t\t<!-- .employee  -->
\t\t\t\t<div class=\"col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container\">
\t\t\t\t\t<div class=\"thumb-info\">
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t<!-- Image -->
\t\t\t\t\t\t\t<img src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/about/james-whitfill-thumb.jpg"), "html", null, true);
        echo "\" alt=\"\" title=\"\" width=\"270\" height=\"270\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"description-container\">
\t\t\t\t\t\t\t<div class=\"description\">
\t\t\t\t\t\t\t\t<!-- Name -->
\t\t\t\t\t\t\t\t<h2 class=\"name\">James Whitfill</h2>
\t\t\t\t\t\t\t\t<!-- Designation -->
\t\t\t\t\t\t\t\t<p class=\"role\">MD</p>
\t\t\t\t\t\t\t\t<p><a href=\"#james-whitfill\" data-toggle=\"modal\" data-target=\"#james-whitfill\"><i class=\"fa fa-info-circle fa-2x white\"></i></a></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<h6>Chief Medical Officer</h6>
\t\t\t\t</div>
\t\t\t\t<!-- .employee -->
\t\t\t\t<div class=\"col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container\">
\t\t\t\t\t<div class=\"thumb-info\">
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t<!-- Image -->
\t\t\t\t\t\t\t<img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/about/faron-thompson-thumb.jpg"), "html", null, true);
        echo "\" alt=\"\" title=\"\" width=\"270\" height=\"270\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"description-container\">
\t\t\t\t\t\t\t<div class=\"description\">
\t\t\t\t\t\t\t\t<!-- Name -->
\t\t\t\t\t\t\t\t<h2 class=\"name\">Faron Thompson</h2>
\t\t\t\t\t\t\t\t<!-- Designation -->
\t\t\t\t\t\t\t\t<p class=\"role\">MBA</p>
\t\t\t\t\t\t\t\t<p><a href=\"#faron-thompson\" data-toggle=\"modal\" data-target=\"#faron-thompson\"><i class=\"fa fa-info-circle fa-2x white\"></i></a></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<h6>Chief Operating Officer</h6>
\t\t\t\t</div>
\t\t\t\t<!-- .employee -->
\t\t\t\t<div class=\"col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container\">
\t\t\t\t\t<div class=\"thumb-info\">
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t<!-- Image -->
\t\t\t\t\t\t\t<img src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/site/images/sections/about/karen-r-vanaskie-thumb.jpg"), "html", null, true);
        echo "\" alt=\"\" title=\"\" width=\"270\" height=\"270\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"description-container\">
\t\t\t\t\t\t\t<div class=\"description\">
\t\t\t\t\t\t\t\t<!-- Name -->
\t\t\t\t\t\t\t\t<h2 class=\"name\">Karen Vanaskie</h2>
\t\t\t\t\t\t\t\t<!-- Designation -->
\t\t\t\t\t\t\t\t<p class=\"role\">DNP, MSN, RN</p>
\t\t\t\t\t\t\t\t<p><a href=\"#karen-r-vanaskie\" data-toggle=\"modal\" data-target=\"#karen-r-vanaskie\"><i class=\"fa fa-info-circle fa-2x white\"></i></a></p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<h6>Care Management Program Director</h6>
\t\t\t</div>
\t\t</div>
\t</section>

\t<div id=\"tiffany-nelson\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"vertical-alignment-helper\">
\t\t\t<div class=\"modal-dialog vertical-align-center\" role=\"document\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t\t\t\t<h3 id=\"myModalLabel\">Tiffany Nelson <span>MD, Chief Strategy Officer</span></h3>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<p>Dr. Tiffany Nelson played an integral role in the formation of ";
        // line 113
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " (SHP).  She currently serves as the Chief Strategy Officer, and has been in the role of Chairman of the Board and Chief Medical Officer.  Dr. Nelson has been an instrumental part of developing both the care management program and technology platform that has been critical to the success of SHP.  Dr. Nelson obtained her medical degree from The University of Arizona College of Medicine, where she received the prestigious Alpha Omega Alpha distinction.  Following medical school, Dr. Nelson completed her residency training in Family Medicine at Banner Good Samaritan where she served as Chief Resident.  Dr. Nelson was chosen to be part of the inaugural class of Health IT Fellows by ONC.</p>
\t\t\t\t\t\t<p>Dr. Nelson is the founder of Desert Ridge Family Physicians in Phoenix, AZ.  The practice was created in 2004 to promote a patient-centered philosophy in order to foster long lasting relationships with their patients.  Desert Ridge Family Physicians has grown to ten physicians and provides care to more than 25,000 patients.  The practice has received NextGen’s Best Small Practice Award, Intel Innovation Award and Arizona Republic’s Most Family Friendly Primary Care Offic</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"james-whitfill\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"vertical-alignment-helper\">
\t\t\t<div class=\"modal-dialog vertical-align-center\" role=\"document\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t\t\t\t<h3 id=\"myModalLabel\">James Whitfill <span>MD, Chief Medical Officer</span></h3>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<p>Dr. Whitfill is currently the Chief Medical Officer for ";
        // line 130
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo ", a Clinical Integrated Network in Phoenix Arizona. Dr. Whitfill received his Bachelor of Arts degree from Princeton University and his Medical Degree from the University of Pennsylvania. He did his Residency and Chief Residency in Internal Medicine at the Hospital of the University of Pennsylvania and completed a fellowship in Medical Informatics in the University of Pennsylvania Department of Medicine.</p>
\t\t\t\t\t\t<p>His focus has been on the use of business intelligence for clinical and operational improvement within healthcare organizations; leadership and team development; and workflow-focused information technology. He currently serves as a Clinical Associate Professor, Departments of Internal Medicine and Bioinformatics at the University of Arizona College of Medicine-Phoenix.  He frequently lectures on the intersection of population health and information technology.  He previously held and currently holds advisory board responsibilities at GE Healthcare, Philips Healthcare, IDX, KLAS and Society of Imaging Informatics in Medicine. In recent years, he has transitioned his focus to include population health.</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"faron-thompson\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"vertical-alignment-helper\">
\t\t\t<div class=\"modal-dialog vertical-align-center\" role=\"document\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t\t\t\t<h3 id=\"myModalLabel\">Faron Thompson <span>MBA, Chief Operating Officer</span></h3>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<p>Faron Thompson is the Chief Operating Officer for ";
        // line 147
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo " (SHP). As COO, he is responsible for Information Technology, Finance, and Operations for SHP. He joined SHP as Chief Information Officer in September, 2012. Prior to this date, he served SHP in a similar capacity as a consultant leading the selection and implementation of technologies to help start up ";
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo ". Faron has over 20 years of experience in Healthcare leading information technology, process improvement, reengineering, cost improvement, strategy, and merger & acquisition projects. Additionally, he has over eight years of experience in Financial Services, Consumer Products, Manufacturing, and Education. Faron started his career in Healthcare consulting with Ernst & Young in St. Louis, MO where he received his MBA from Washington University in 1992.</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<div id=\"karen-r-vanaskie\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"vertical-alignment-helper\">
\t\t\t<div class=\"modal-dialog vertical-align-center\" role=\"document\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
\t\t\t\t\t\t<h3 id=\"myModalLabel\">Karen Vanaskie <span>DNP, MSN, RN, Care Management Program Director</span></h3>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<p>Karen Vanaskie currently serves as Care Management Program Director for ";
        // line 163
        echo twig_escape_filter($this->env, (isset($context["company_name"]) ? $context["company_name"] : $this->getContext($context, "company_name")), "html", null, true);
        echo ". Over the span of her career, Karen holds 30 years of nursing experience along with extensive experience in acute care settings, home health, multi-specialty medical groups, independent physician associations, and managed care organizations. Karen has spent her nursing leadership career developing case management programs, overseeing quality of care, infection control, and risk management activities, and serving as a chief nursing officer. Karen’s formal education includes earning a Baccalaureate Degree in the Science of Nursing (B.S.N.), a Master’s Degree in the Science of Nursing (M.S.N.), and a Doctorate in Nursing Practice (D.N.P).</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

";
        
        $__internal_19a518478b58420a1e6c036081b74ddb78ce451f2ee16342e2fbb5900351e1fc->leave($__internal_19a518478b58420a1e6c036081b74ddb78ce451f2ee16342e2fbb5900351e1fc_prof);

    }

    public function getTemplateName()
    {
        return "SiteBundle:About:executiveTeam.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 163,  227 => 147,  207 => 130,  187 => 113,  157 => 86,  135 => 67,  113 => 48,  91 => 29,  78 => 21,  66 => 12,  62 => 10,  56 => 9,  44 => 7,  37 => 5,  35 => 4,  33 => 3,  31 => 2,  29 => 1,  11 => 5,);
    }
}
/* {% set title = 'Executive Team' %}*/
/* {% set meta_keywords = null %}*/
/* {% set meta_description = null %}*/
/* {% set meta_author = null %}*/
/* {% extends "::base.html.twig" %}*/
/* */
/* {% block title %}SiteBundle:About:executiveTeams{% endblock %}*/
/* */
/* {% block body %}*/
/* 	<div class="page-header">*/
/* 		<div class="container">*/
/* 			<h1 class="title">{{ title }}</h1>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- page-header -->*/
/* 	<section id="executive-team" class="page-section">*/
/* 		<div class="container text-center">*/
/* 			<div class="col-sm-12 col-md-12">*/
/* 				<div class="section-title">*/
/* 					<!-- Heading -->*/
/* 					<h2 class="title">Meet the {{ company_name }} {{ title }}</h2>*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				<div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container">*/
/* 					<div class="thumb-info">*/
/* 						<div class="image">*/
/* 							<!-- Image -->*/
/* 							<img src="{{ asset('bundles/site/images/sections/about/tiffany-nelson-thumb.jpg') }}" alt="" title="" width="270" height="270" />*/
/* 						</div>*/
/* 						<div class="description-container">*/
/* 							<div class="description">*/
/* 								<!-- Name -->*/
/* 								<h2 class="name">Tiffany Nelson</h2>*/
/* 								<!-- Designation -->*/
/* 								<p class="role">MD</p>*/
/* 								<p><a href="#tiffany-nelson" data-toggle="modal" data-target="#tiffany-nelson"><i class="fa fa-info-circle fa-2x white"></i></a></p>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<h6>Chief Strategy Officer</h6>*/
/* 				</div>*/
/* 				<!-- .employee  -->*/
/* 				<div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container">*/
/* 					<div class="thumb-info">*/
/* 						<div class="image">*/
/* 							<!-- Image -->*/
/* 							<img src="{{ asset('bundles/site/images/sections/about/james-whitfill-thumb.jpg') }}" alt="" title="" width="270" height="270" />*/
/* 						</div>*/
/* 						<div class="description-container">*/
/* 							<div class="description">*/
/* 								<!-- Name -->*/
/* 								<h2 class="name">James Whitfill</h2>*/
/* 								<!-- Designation -->*/
/* 								<p class="role">MD</p>*/
/* 								<p><a href="#james-whitfill" data-toggle="modal" data-target="#james-whitfill"><i class="fa fa-info-circle fa-2x white"></i></a></p>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<h6>Chief Medical Officer</h6>*/
/* 				</div>*/
/* 				<!-- .employee -->*/
/* 				<div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container">*/
/* 					<div class="thumb-info">*/
/* 						<div class="image">*/
/* 							<!-- Image -->*/
/* 							<img src="{{ asset('bundles/site/images/sections/about/faron-thompson-thumb.jpg') }}" alt="" title="" width="270" height="270" />*/
/* 						</div>*/
/* 						<div class="description-container">*/
/* 							<div class="description">*/
/* 								<!-- Name -->*/
/* 								<h2 class="name">Faron Thompson</h2>*/
/* 								<!-- Designation -->*/
/* 								<p class="role">MBA</p>*/
/* 								<p><a href="#faron-thompson" data-toggle="modal" data-target="#faron-thompson"><i class="fa fa-info-circle fa-2x white"></i></a></p>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<h6>Chief Operating Officer</h6>*/
/* 				</div>*/
/* 				<!-- .employee -->*/
/* 				<div class="col-sm-4 col-md-3 icons-hover-color bottom-xs-pad-20 info-thumb-container">*/
/* 					<div class="thumb-info">*/
/* 						<div class="image">*/
/* 							<!-- Image -->*/
/* 							<img src="{{ asset('bundles/site/images/sections/about/karen-r-vanaskie-thumb.jpg') }}" alt="" title="" width="270" height="270" />*/
/* 						</div>*/
/* 						<div class="description-container">*/
/* 							<div class="description">*/
/* 								<!-- Name -->*/
/* 								<h2 class="name">Karen Vanaskie</h2>*/
/* 								<!-- Designation -->*/
/* 								<p class="role">DNP, MSN, RN</p>*/
/* 								<p><a href="#karen-r-vanaskie" data-toggle="modal" data-target="#karen-r-vanaskie"><i class="fa fa-info-circle fa-2x white"></i></a></p>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				<h6>Care Management Program Director</h6>*/
/* 			</div>*/
/* 		</div>*/
/* 	</section>*/
/* */
/* 	<div id="tiffany-nelson" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">*/
/* 		<div class="vertical-alignment-helper">*/
/* 			<div class="modal-dialog vertical-align-center" role="document">*/
/* 				<div class="modal-content">*/
/* 					<div class="modal-header">*/
/* 						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/* 						<h3 id="myModalLabel">Tiffany Nelson <span>MD, Chief Strategy Officer</span></h3>*/
/* 					</div>*/
/* 					<div class="modal-body">*/
/* 						<p>Dr. Tiffany Nelson played an integral role in the formation of {{ company_name }} (SHP).  She currently serves as the Chief Strategy Officer, and has been in the role of Chairman of the Board and Chief Medical Officer.  Dr. Nelson has been an instrumental part of developing both the care management program and technology platform that has been critical to the success of SHP.  Dr. Nelson obtained her medical degree from The University of Arizona College of Medicine, where she received the prestigious Alpha Omega Alpha distinction.  Following medical school, Dr. Nelson completed her residency training in Family Medicine at Banner Good Samaritan where she served as Chief Resident.  Dr. Nelson was chosen to be part of the inaugural class of Health IT Fellows by ONC.</p>*/
/* 						<p>Dr. Nelson is the founder of Desert Ridge Family Physicians in Phoenix, AZ.  The practice was created in 2004 to promote a patient-centered philosophy in order to foster long lasting relationships with their patients.  Desert Ridge Family Physicians has grown to ten physicians and provides care to more than 25,000 patients.  The practice has received NextGen’s Best Small Practice Award, Intel Innovation Award and Arizona Republic’s Most Family Friendly Primary Care Offic</p>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<div id="james-whitfill" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">*/
/* 		<div class="vertical-alignment-helper">*/
/* 			<div class="modal-dialog vertical-align-center" role="document">*/
/* 				<div class="modal-content">*/
/* 					<div class="modal-header">*/
/* 						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/* 						<h3 id="myModalLabel">James Whitfill <span>MD, Chief Medical Officer</span></h3>*/
/* 					</div>*/
/* 					<div class="modal-body">*/
/* 						<p>Dr. Whitfill is currently the Chief Medical Officer for {{ company_name }}, a Clinical Integrated Network in Phoenix Arizona. Dr. Whitfill received his Bachelor of Arts degree from Princeton University and his Medical Degree from the University of Pennsylvania. He did his Residency and Chief Residency in Internal Medicine at the Hospital of the University of Pennsylvania and completed a fellowship in Medical Informatics in the University of Pennsylvania Department of Medicine.</p>*/
/* 						<p>His focus has been on the use of business intelligence for clinical and operational improvement within healthcare organizations; leadership and team development; and workflow-focused information technology. He currently serves as a Clinical Associate Professor, Departments of Internal Medicine and Bioinformatics at the University of Arizona College of Medicine-Phoenix.  He frequently lectures on the intersection of population health and information technology.  He previously held and currently holds advisory board responsibilities at GE Healthcare, Philips Healthcare, IDX, KLAS and Society of Imaging Informatics in Medicine. In recent years, he has transitioned his focus to include population health.</p>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<div id="faron-thompson" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">*/
/* 		<div class="vertical-alignment-helper">*/
/* 			<div class="modal-dialog vertical-align-center" role="document">*/
/* 				<div class="modal-content">*/
/* 					<div class="modal-header">*/
/* 						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/* 						<h3 id="myModalLabel">Faron Thompson <span>MBA, Chief Operating Officer</span></h3>*/
/* 					</div>*/
/* 					<div class="modal-body">*/
/* 						<p>Faron Thompson is the Chief Operating Officer for {{ company_name }} (SHP). As COO, he is responsible for Information Technology, Finance, and Operations for SHP. He joined SHP as Chief Information Officer in September, 2012. Prior to this date, he served SHP in a similar capacity as a consultant leading the selection and implementation of technologies to help start up {{ company_name }}. Faron has over 20 years of experience in Healthcare leading information technology, process improvement, reengineering, cost improvement, strategy, and merger & acquisition projects. Additionally, he has over eight years of experience in Financial Services, Consumer Products, Manufacturing, and Education. Faron started his career in Healthcare consulting with Ernst & Young in St. Louis, MO where he received his MBA from Washington University in 1992.</p>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<div id="karen-r-vanaskie" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">*/
/* 		<div class="vertical-alignment-helper">*/
/* 			<div class="modal-dialog vertical-align-center" role="document">*/
/* 				<div class="modal-content">*/
/* 					<div class="modal-header">*/
/* 						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>*/
/* 						<h3 id="myModalLabel">Karen Vanaskie <span>DNP, MSN, RN, Care Management Program Director</span></h3>*/
/* 					</div>*/
/* 					<div class="modal-body">*/
/* 						<p>Karen Vanaskie currently serves as Care Management Program Director for {{ company_name }}. Over the span of her career, Karen holds 30 years of nursing experience along with extensive experience in acute care settings, home health, multi-specialty medical groups, independent physician associations, and managed care organizations. Karen has spent her nursing leadership career developing case management programs, overseeing quality of care, infection control, and risk management activities, and serving as a chief nursing officer. Karen’s formal education includes earning a Baccalaureate Degree in the Science of Nursing (B.S.N.), a Master’s Degree in the Science of Nursing (M.S.N.), and a Doctorate in Nursing Practice (D.N.P).</p>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* {% endblock %}*/
/* */
